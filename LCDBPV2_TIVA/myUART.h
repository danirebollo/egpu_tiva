/*
 * myUART.h
 *
 *  Created on: 10/10/2015
 *      Author: Dani
 */

#ifndef MYUART_H_
#define MYUART_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "stdio.h"

#include <time.h>
//#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
//#include "inc/hw_ints.h"

#include "utils/ustdlib.h"
#include "utils/uartstdio.h"
//#include "uartstdio.h"

#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/udma.h"
#include "driverlib/uart.h"

#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/pushbutton.h"

#include "drivers/LCDBPV2_setup.h"
#include "drivers/LCDBP320x240x16_SSD1289.h"
#include "drivers/LCDBPV2_backlight.h"
#include "drivers/LCDBPV2_touch.h"

void initUART();
void initUARTmessage();
void processUART();
void sendi2c(int ni2c, char addr);
void receivei2c(int ni2c, char addr);
void UART_I_PRINT(int pcFormat);
void UART_I_PRINTLN(int pcFormat);
void UART_C_PRINT(const char *pcFormat);
void UART_C_PRINTLN(const char *pcFormat);
void UART_H_PRINT(char pcFormat);
void UART_H_PRINTLN(char pcFormat);
void UART_L_PRINT(unsigned long x);
void UART_L_PRINTLN(unsigned long x);



#endif /* MYUART_H_ */
