/*
 * LCDBPV2_touch.c
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#include "LCDBPV2_touch.h"
#include "LCDBPV2_setup.h"
#include "string.h"

//Function that handles Timer0 Interrupt.
//
//It is called every ulPeriod period after being activated by touch_IntHandler() (LCDBPV2_touch.c) when stylus press the screen.
//Timer0 Interrupt is disabled when stylus leaves the screen.
//// TimerLoadSet(LCD_TIMER0_BASE, TIMER_A, ulPeriod); sets period to the new ulPeriod. (default ulPeriod=SysCtlClockGet()/800)
//void Timer0IntHandler(void);
//void LCDBPV2_touch_ConfigI2C();
extern char widget_touch_state;
uint16_t tX,tY;
int ReceiveI2C(int op);
//
//stores uncalibrated data
uint16_t tYc, tXc;
//ask touch data and calibrates this. Stores X on tX, Y on tY, Z1 on tZ1, Z2 on tZ2 and Rtouch on tRtouch.
void gettouch();
//process events to be executed every touch

//flag that stores if touch driver enter into interrupt for the first time after falling edge or not.
char fldown;
//flag that avoid to recalibrate the same data repeatedly
char flcc;

long tempparm[7];

static int32_t (*g_pfnTSHandler)(uint32_t ui32Message, int32_t i32X, int32_t i32Y);

//*****************************************************************************
// A pointer to the function to receive messages from the touch screen driver
// when events occur on the touch screen (debounced presses, movement while
// pressed, and debounced releases).
//*****************************************************************************
//static int32_t (*g_pfnTSHandler)(uint32_t ulMessage, int32_t lX, int32_t lY);

//function recalculates coordenates (raw data) with calibrated data.
void LCDBPV2_calcParam();

//Stores previous X and Y data
uint16_t xbk,ybk;

//create a new type of variable that stores functions.
//typedef void (*functiontype2)();

//stores events (functions) that can be called when touch
functiontype2 touchevents[100];
//send function pointer to be stored into an event to be called when touch.
void addevent(functiontype2 event1);
//count the number of events (functions) to be executed when touch. This variable increases when add a new event.
int tevents;

// A pointer to the current touchscreen calibration parameter set.
const long *g_plParmSet;

volatile unsigned char g_cCalibrationMode;

unsigned short g_psCalLCD[3][2];	// 3x x/y LCD
unsigned short g_psCalRAW[3][2];	// 3x x/y RAW

long g_plCalibrationMatrix[7];	// 2x3 matrix and divider calculated
///////////////////
char bussyint;

//MCU specific
void sendOneI2C(char message);
uint16_t readXY();

int32_t TSHandler(uint32_t ui32Message, int32_t i32X, int32_t i32Y);
//*****************************************************************************
//
// A pointer to the function to receive messages from the touch screen driver
// when events occur on the touch screen (debounced presses, movement while
// pressed, and debounced releases).
//
//*****************************************************************************
void TouchScreenCallbackSet(int32_t (*pfnCallback)(uint32_t ui32Message, int32_t i32X, int32_t i32Y))
{
    // Save the pointer to the callback function.
    g_pfnTSHandler = pfnCallback;
}
char touchMode;
//Init of the touch driver. User must call this function to enable touch.
void LCDBPV2_touch_Init()
{
	//set address
	#ifdef LCDBPV2RevC
	RevC_SetTOUCHFlags(0x08);
	#else
	sendOneI2C(0xB0);
	#endif


	//Activate Y+, X-drivers: 1010+00(Power down between cycles. PENIRQ enabled)+0(12bit mode)+0(dont care)
	//start
	//send address byte I2C_SLAVE_ADDRESS
	//I2CMasterSlaveAddrSet( I2C_M_BASE, TOUCH_I2C_ADDRESS, false);
	////send command byte
	//I2CMasterDataPut( I2C_M_BASE, 0xA0);
	//I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
	//WaitI2CDone( I2C_M_BASE);
	//stop
	//initializes events counter.
	tevents=0;
	//set "data needs to be calibrated"
	flcal=0;
	//set I2C error flag to 0
	I2Cwd=0;
	bussyint=0;
	touchMode=TOUCH_SIMPLE;
	tX=0;
	tY=0;

	//dummy write to set low power mode
	//**power down between cycles (low power mode) 	00:c0
	//**A/D on, IRQ disabled 						01:c4
	//**A/D off, IRQ enabled 						10:c8
	//**A/D on, IRQ disabled 						11:cc
//	tY=LCDBPV2_touch_receiveI2C(0xc0);

#if defined(MSP430G2) || defined(__MSP430F5529__)
#else

	TouchScreenCallbackSet(WidgetPointerMessage);
#endif

	gettouch();

}
//send a command to touch driver and receives and acknowledge (x,y,z coordenates)
uint16_t LCDBPV2_touch_receiveI2C(char op) //int???? --> cambiar a char
{
	sendOneI2C(op);
	return readXY();
}

//Handles touch interrupt
char touch_state()
{
	   if (LCD_READINT()==0) //(HWREG(LCD_INT_BASE +  ((LCD_INT_PIN) << 2))==0)
	   {
		   return TOUCH_DOWN;
	   }
	   else
	   {
		   return TOUCH_UP;
	   }
}

void touch_IntHandler(void) //button x=330, y=250  x=53, y=257
{
	LCD_GPIOINT_CLR();
	LCD_GPIOINT_DISABLE();

	intstate=0;
	_SysCtlDelay(10);
	_SysCtlDelay(10);
	//_SysCtlDelay(10000);

   if (touch_state()==TOUCH_DOWN)
   {
	   _SysCtlDelay(100);
	   touchprocess(TOUCH_DOWN);
	   _SysCtlDelay(1000);
	   		#if defined (TOUCH_DEBUG)
		   	   GPIOPinWrite(LED_PORT, GREEN_LED, GREEN_LED);
		   	   _UART_C_PRINTLN("INT0.");
	   	   	#endif

	   LCD_TIMERINT_CLR();
	   _SysCtlDelay(100);
	   LCD_TIMERINT_ENABLE();
	   _SysCtlDelay(100);
   }
   else //TOUCH_UP
   {
		#if defined (TOUCH_DEBUG)
	   	   GPIOPinWrite(LED_PORT, GREEN_LED, 0);
	   	   _UART_C_PRINTLN("INT1.");
  		#endif

	   LCD_GPIOINT_CLR();
	   LCD_GPIOINT_ENABLE();
	   _SysCtlDelay(100);

	   _SysCtlDelay(10);
	 	   intstate=1;
   }
}



void LCDBPV2_touchMode(char _touchmode)
{
	touchMode=_touchmode;
}
void tProcess(int message, int X, int Y)
{
		g_pfnTSHandler(message, X, Y);
}
//process events to be executed every touch
void touchprocess(char state)
{
	_SysCtlDelay(10000);
	if(state==laststate && state==TOUCH_DOWN) //&& state==TOUCH_DOWN
	{
#if defined TOUCH_DEBUG
		testLed(LED_1, 500);
#endif

		if ((touchMode==TOUCH_PERSISTENT_MOVE) || (touchMode==TOUCH_PERSISTENT_DOWN))
		{

			gettouch();
			if((tX!=xbk)||(tY!=ybk))
			{
				//_UART_C_PRINTLN("TOUCH persistive");
				if (touchMode==TOUCH_PERSISTENT_MOVE)
				{
					tProcess(WIDGET_MSG_PTR_MOVE, tX, tY);
				}
				else // TOUCH_PERSISTENT_DOWN
				{
					tProcess(WIDGET_MSG_PTR_DOWN, tX, tY);
				}
				xbk=tX;
				ybk=tY;
				laststate=state;
			}
		}
		//_SysCtlDelay(10000000);
	}
	else
	{
		if(state==TOUCH_UP)
		{
			//_UART_C_PRINTLN("TOUCH up");
#if defined TOUCH_DEBUG
			testLed(LED_2, 500);
#endif
			tProcess(WIDGET_MSG_PTR_UP, xbk, ybk);
			laststate=state;
		}
		else //TOUCH_DOWN
		{
			gettouch();
#if defined TOUCH_DEBUG
			testLed(LED_3, 500);
#endif
		//	if(tRtouch<TOUCH_THRESHOLD) // tRtouch<TOUCH_THRESHOLD //&&g_pfnTSHandler tRtouch<TOUCH_THRESHOLD
		//	{
				//_UART_C_PRINTLN("TOUCH down");
				if((tX!=xbk)||(tY!=ybk))
				{
					/*
						int jj;
						for(jj=0;jj<tevents;jj++)
						{
							touchevents[jj]();
						}
					 */
					xbk=tX;
					ybk=tY;
					tProcess(WIDGET_MSG_PTR_DOWN, tX, tY);
				}
				else
				{

				}
				laststate=state;
		//	}
		}
	}
	_SysCtlDelay(1000);
}



//ask touch data and calibrates this. Stores X on tX, Y on tY, Z1 on tZ1, Z2 on tZ2 and Rtouch on tRtouch.
void gettouch()
{
	//		MODE		D7-D4 	D3-D2	D1	D0
	//C8	Measure X  (1100)	10		0	Dont care (0)
	//D8	Measure Y  (1101)	10		0	Dont care (0)
	//F9	Measure Z1 (1110)	10		0	Dont care (0)
	//E8	Measure Z2 (1111)	10		0	Dont care (0)

	//TimerIntDisable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
	//TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
	_SysCtlDelay(100);

#ifdef LCDBPV2RevC //XPT

		char x0, y0;
		int x1,y1;

		RevC_GetTouchRAWXY();

		LCDBPV2_Get4();

		x0=d0;
		x1=((char)((d1 & 0xF0) >> 4))<<8;

		y0=(char) (d1 & 0x0F);
		y1=d2<<4;

		tX=x0+x1;
		tY=y0+y1;
		tXc=tX;
		tYc=tY;
	///////////////////////////////////////////////////////////////////////////////
		RevC_GetTouchRAWZ1Z2();
		LCDBPV2_Get4(&d0,&d1,&d2,&d3);

			x0=d0;
			x1=((char)((d1 & 0xF0) >> 4))<<8;
			y0=(char) (d1 & 0x0F);
			y1=d2<<4;

			tZ1=x0+x1;
			tZ2=y0+y1;
#else	//TSC
			tY=LCDBPV2_touch_receiveI2C(0xC4); //c0 c8 c1
			_SysCtlDelay(10);
				tX=LCDBPV2_touch_receiveI2C(0xD4);
				_SysCtlDelay(10);
				tZ1=LCDBPV2_touch_receiveI2C(0xf4); //f4
				_SysCtlDelay(10);
				tZ2=LCDBPV2_touch_receiveI2C(0xe4); //e8
				_SysCtlDelay(10);
				tXc=tX;
				tYc=tY;
			sendOneI2C(0xe8);//E0 e8
#endif



	//!XPT

	//****calc tRtouch
	//tRtouch=((tX*(tZ1-tZ2))/(4096*tZ2));
	tRtouch=(350*tX/4096)*((tZ1/tZ2)-1);

	//if flcal==1 raw data is requested by calibrate function. If 0 raw data is calibrated.
	if(flcal==0)
	{
		LCDBPV2_calcParam();
		if(tY<0) tY=0;
		if(tY>LCD_MAX_Y) tY=LCD_MAX_Y;
		if(tX<0) tX=0;
		if(tX>LCD_MAX_X) tX=LCD_MAX_X;
	}

}

void addevent(functiontype2 event1)
{
	touchevents[tevents]=event1;
	tevents++;
}

//*****************************************************************************
//
//! Waits for calibration point to be pressed by user.
//!
//! This function sets the driver into calibration mode until the next pen
//! up event happens. The reading is stored for the provided point index.
//! After calling this function for point 0, 1 and 2, call TouchScreenCalibrate
//! to calculate new calibration matrix.
//!
//! \param sPointX	X screen coordinate of calibration point
//! \param sPointY	Y screen coordinate of calibration point
//! \param ulPointIndex Number of calibration point 0-2
//!
//! \return None.
//
//*****************************************************************************
void TouchScreenCalibrationPoint(uint16_t sPointX, uint16_t sPointY, uint16_t ulPointIndex)
{
	//set calibration state on. In this state raw data is not processed with any calibration data.
	flcal=1;
	//whait until touchstate==1
	do
	{
		_SysCtlDelay(200);
	}while(touch_state()!=TOUCH_DOWN);

	//(touch Interrupt has occurred and gpio pin is in low state. RAW data has been read (tX and tY). Has not taken place a rising edge yet.)

	//whait until touchstate==2.
	do
	{
		_SysCtlDelay(200);
	}while(touch_state()!=TOUCH_UP);
	//(touch Interrupt has occurred and gpio pin is in high state. Has occurred a rising edge.)


	if(ulPointIndex <= 3)
	{
		// store LCD coordinates and raw touch data for later processing
		g_psCalLCD[ulPointIndex][0] = sPointX;
		g_psCalLCD[ulPointIndex][1] = sPointY;
		g_psCalRAW[ulPointIndex][0] = tXc;//g_sCalibrateX
		g_psCalRAW[ulPointIndex][1] = tYc;//g_sCalibrateY
	}
	flcal=0;
}

//*****************************************************************************
//
//! Calculates calibration matrix from calibration points
//!
//! This function calculates the calibration matrix from information collected
//! from preceding calls to TouchScreenCalibrationPoint.
//! If calibration was successful, the touch driver will use the new
//! calibration matrix.
//!
//! \return Returns pointer to calibration matrix if successful, 0 otherwise.
//
//*****************************************************************************
long* LCDBPV2_TouchScreenCalibrate(void)
{
	// Calculate calibration matrix using collected calibration data

	// Calculate divider value
	g_plCalibrationMatrix[6] = ((g_psCalRAW[0][0] - g_psCalRAW[2][0]) * (g_psCalRAW[1][1] - g_psCalRAW[2][1])) - ((g_psCalRAW[1][0] - g_psCalRAW[2][0]) * (g_psCalRAW[0][1] - g_psCalRAW[2][1]));
	ret[6]=g_plCalibrationMatrix[6];

	if(g_plCalibrationMatrix[6] == 0)
	{
		// fatal error, divider can't be 0
		return(0);
	}

	// Calculate A
	g_plCalibrationMatrix[0] =((g_psCalLCD[0][0] - g_psCalLCD[2][0]) * (g_psCalRAW[1][1] - g_psCalRAW[2][1])) -((g_psCalLCD[1][0] - g_psCalLCD[2][0]) * (g_psCalRAW[0][1] - g_psCalRAW[2][1]));
	ret[0]=g_plCalibrationMatrix[0];

	// Calculate B
	g_plCalibrationMatrix[1] =((g_psCalRAW[0][0] - g_psCalRAW[2][0]) * (g_psCalLCD[1][0] - g_psCalLCD[2][0])) -((g_psCalLCD[0][0] - g_psCalLCD[2][0]) * (g_psCalRAW[1][0] - g_psCalRAW[2][0]));
	ret[1]=g_plCalibrationMatrix[1];

	// Calculate C
	g_plCalibrationMatrix[2] =(g_psCalRAW[2][0] * g_psCalLCD[1][0] - g_psCalRAW[1][0] * g_psCalLCD[2][0]) * g_psCalRAW[0][1] + (g_psCalRAW[0][0] * g_psCalLCD[2][0] -
			g_psCalRAW[2][0] * g_psCalLCD[0][0]) * g_psCalRAW[1][1] +(g_psCalRAW[1][0] * g_psCalLCD[0][0] - g_psCalRAW[0][0] * g_psCalLCD[1][0]) * g_psCalRAW[2][1];
	ret[2]=g_plCalibrationMatrix[2];

	// Calculate D
	g_plCalibrationMatrix[3] =((g_psCalLCD[0][1] - g_psCalLCD[2][1]) * (g_psCalRAW[1][1] - g_psCalRAW[2][1])) -((g_psCalLCD[1][1] - g_psCalLCD[2][1]) * (g_psCalRAW[0][1] - g_psCalRAW[2][1]));
	ret[3]=g_plCalibrationMatrix[3];

	// Calculate E
	g_plCalibrationMatrix[4] =((g_psCalRAW[0][0] - g_psCalRAW[2][0]) * (g_psCalLCD[1][1] - g_psCalLCD[2][1])) -((g_psCalLCD[0][1] - g_psCalLCD[2][1]) * (g_psCalRAW[1][0] - g_psCalRAW[2][0]));
	ret[4]=g_plCalibrationMatrix[4];

	// Calculate F
	g_plCalibrationMatrix[5] =(g_psCalRAW[2][0] * g_psCalLCD[1][1] - g_psCalRAW[1][0] * g_psCalLCD[2][1]) * g_psCalRAW[0][1] +(g_psCalRAW[0][0] * g_psCalLCD[2][1] -
			g_psCalRAW[2][0] * g_psCalLCD[0][1]) * g_psCalRAW[1][1] +(g_psCalRAW[1][0] * g_psCalLCD[0][1] - g_psCalRAW[0][0] * g_psCalLCD[1][1]) * g_psCalRAW[2][1];
	ret[5]=g_plCalibrationMatrix[5];

	// Update touch driver to use new matrix
	g_plParmSet = g_plCalibrationMatrix;
	// calibration successful
	return(g_plCalibrationMatrix);
}

//function recalculates coordenates (raw data) with calibrated data.
void LCDBPV2_calcParam()
{
		long lTemp;
		lTemp = (((tX * g_plParmSet[0]) + (tY * g_plParmSet[1]) + g_plParmSet[2]) /g_plParmSet[6]);
		tY = (((tX * g_plParmSet[3]) + (tY * g_plParmSet[4]) + g_plParmSet[5]) /g_plParmSet[6]);
		tX = lTemp;
}

void LCDBPV2_setparm(long *parm)
{
	g_plParmSet=parm;
}

long* LCDBPV2_loadparm()
{

	//A
	tempparm[0]=49544; 		//392712		192280				367312
	//B
	tempparm[1]=24440;			//-2480			1528				-14920
	//C
	tempparm[2]=-226467712;	//-1531654288		-383161456		-1418507328
	//D
	tempparm[3]=-263472;			//-8144			-4240				-9056
	//E
	tempparm[4]=-39120;			//289440		146736				285360
	//F
	tempparm[5]=851316096;	//-1128642616		-282237392		-1051324456
	//Div
	tempparm[6]=-175824;		//-4440889		-1102378			-4089103

	g_plParmSet=tempparm;
	return(tempparm);

}

//////////////////////////////////////////////////////////////
// ***********************************************************


void sendOneI2C(char message)
{
	LCD_sendoneI2C(message);
}
uint16_t readXY()
{
	return LCD_READXY();
}

