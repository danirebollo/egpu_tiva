/*
 * LCDBPV2_MCU.c
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#include "LCDBPV2_MCU.h"


void LCD_D0_S()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = LCD_SDATA0_PIN;
	HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = LCD_SDATA0_PIN;
#elif defined(MSP430G2)
	LCD_SET(LCD_D0);
#elif defined(__MSP430F5529__)

#endif
}
void LCD_D0_R()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = 0;
	HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = 0;
#elif defined(MSP430G2)
	LCD_RESET(LCD_D0);
#elif defined(__MSP430F5529__)

#endif
}

void LCD_D1_S()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = LCD_SDATA1_PIN;
	HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = LCD_SDATA1_PIN;
#elif defined(MSP430G2)
	LCD_SET(LCD_D1);
#elif defined(__MSP430F5529__)

#endif
}
void LCD_D1_R()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = 0;
	HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = 0;
#elif defined(MSP430G2)
	LCD_RESET(LCD_D1);
#elif defined(__MSP430F5529__)

#endif
}

void LCD_CLK_S()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = LCD_SCLK_PIN;
	HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = LCD_SCLK_PIN;
#elif defined(MSP430G2)
	LCD_SET(LCD_CLK);
#elif defined(__MSP430F5529__)

#endif
}
void LCD_CLK_R()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = 0;
	HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = 0;
#elif defined(MSP430G2)
	LCD_RESET(LCD_CLK);
#elif defined(__MSP430F5529__)

#endif
}

void LCD_CS_S()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = LCD_SCS_PIN;
	HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = LCD_SCS_PIN;
#elif defined(MSP430G2)
	LCD_SET(LCD_CS);
#elif defined(__MSP430F5529__)

#endif
}
void LCD_CS_R()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = 0;
	HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = 0;
#elif defined(MSP430G2)
	LCD_RESET(LCD_CS);
#elif defined(__MSP430F5529__)

#endif
}

void LCD_GPIOINT_CLR()
{
#if defined(CC3200)
GPIOIntClear(LCD_INT_GPIO_BASE, LCD_INT_PIN);
#elif defined(TIVA)||defined(TIVACONNECTED)
GPIOIntClear(LCD_INT_BASE, LCD_INT_PIN);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)


#endif
}

void LCD_GPIOINT_DISABLE()
{
#if  defined(CC3200)
GPIOIntDisable(LCD_INT_GPIO_BASE, LCD_INT_PIN);
#elif defined(TIVA)||defined(TIVACONNECTED)
GPIOIntDisable(LCD_INT_BASE, LCD_INT_PIN);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}

void LCD_GPIOINT_ENABLE()
{
#if defined( CC3200)
GPIOIntEnable(LCD_INT_GPIO_BASE, LCD_INT_GPIO_INT_PIN);
#elif defined(TIVA)||defined(TIVACONNECTED)
GPIOIntEnable(LCD_INT_BASE, LCD_INT_PIN);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)


#endif
}


void LCD_TIMERINT_CLR()
{
#if  defined(CC3200)
	   Timer_IF_InterruptClear(LCD_TIMER0_BASE);
#elif defined(TIVA)||defined(TIVACONNECTED)
	   TimerIntClear(LCD_TIMER0_BASE, LCD_TIMER_TIMEOUT);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}

void LCD_TIMERINT_DISABLE()
{
#if defined(CC3200)
	   Timer_IF_Stop(LCD_TIMER0_BASE, TIMER_A);
#elif defined(TIVA)||defined(TIVACONNECTED)
		TimerIntDisable(LCD_TIMER0_BASE, LCD_TIMER_TIMEOUT); //!
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}

void LCD_TIMERINT_ENABLE()
{
#if  defined(CC3200)
	   Timer_IF_Start(LCD_TIMER0_BASE, TIMER_A, touch_period);
#elif defined(TIVA)||defined(TIVACONNECTED)
	   TimerIntEnable(LCD_TIMER0_BASE, LCD_TIMER_TIMEOUT);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}

void LCD_TIMERINT_SETUP()
{
#if ((defined TIVA || defined TIVACONNECTED ))
		SysCtlPeripheralEnable(LCD_TIMER0_PERIPH);
		TimerConfigure(LCD_TIMER0_BASE, TIMER_CFG_PERIODIC); //TIMER_CFG_PERIODIC TIMER_CFG_32_BIT_PER
		#if defined(TOUCH_DEBUG)
		TimerLoadSet(LCD_TIMER0_BASE, LCD_TIMER,TIME_MS*500); //0.04*ulClockMS 1000000 800=0.25ms 1500=0'42ms 3000=91uS 2500=0'152ms
		#else
		TimerLoadSet(LCD_TIMER0_BASE, LCD_TIMER,touch_period);
		#endif

		TimerIntRegister(LCD_TIMER0_BASE, LCD_TIMER,Timer0IntHandler);

		SysCtlDelay(TIME_MS); //needed to avoid faultISR
		IntEnable(LCD_TIMER0);
		SysCtlDelay(TIME_MS);

		TimerEnable(LCD_TIMER0_BASE, LCD_TIMER);
#elif defined(CC3200)
	Timer_IF_Init(PRCM_TIMERA0, TIMERA0_BASE, TIMER_CFG_PERIODIC, TIMER_A, 0);
	Timer_IF_IntSetup(TIMERA0_BASE, TIMER_A, Timer0IntHandler);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
void LCDBPV2_GPIOint_Clear()
{
#if defined(INTSTAT)
if (intstate==1)
{
#if defined (TIVACONNECTEDA)
	GPIOIntClear(LCD_TIMER0_BASE, LCD_TIMER_TIMEOUT);
#endif
}
#endif
}
void LCDBPV2_GPIOint_disable()
{
#if defined(INTSTAT)
	if (intstate==1)
	{
#if defined (TIVACONNECTEDA)
	GPIOIntDisable(LCD_INT_BASE, GPIO_BOTH_EDGES);
#endif
}
#endif
}

void LCDBPV2_GPIOint_enable()
{
#if defined(INTSTAT)
	if (intstate==1)
	{
#if defined (TIVACONNECTEDA)
	GPIOIntEnable(LCD_INT_BASE, GPIO_BOTH_EDGES);
#endif
}
#endif
}

/////////////////////////////////////////////////////

void testLed(int led, int ms)
{
#if defined(TIVA)
	GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, 0x00);
	GPIOPinWrite(LED_PORT, led, led);
	_SysCtlDelay(TIME_MS*ms);
	GPIOPinWrite(LED_PORT, led, 0);
#elif defined(TIVACONNECTED)

	int base;
	int led0;
	if(led==1)
	{
		base=LED_PORT_12;
		led0=LED_1_PIN;
	}
	else if(led==2)
	{
		base=LED_PORT_12;
		led0=LED_2_PIN;
	}
	else if(led==3)
	{
		base=LED_PORT_34;
		led0=LED_3_PIN;
	}
	else
	{
		base=LED_PORT_34;
		led0=LED_4_PIN;
	}

	GPIOPinWrite(base, led0, led0);
	_SysCtlDelay(10000000);
	GPIOPinWrite(base, led0, 0);
	_SysCtlDelay(10000000);
#elif defined(__MSP430F5529__)

#endif

}

unsigned long WaitI2CDone( unsigned int long ulBase)
{
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	// Wait until done transmitting
	while( I2CMasterBusy(I2C_M_BASE));
	// Return I2C error code
	return I2CMasterErr( I2C_M_BASE);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
}

gRectangle LCD_prepareRect(const tRectangle *pRect)
{
	gRectangle srecta;
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	if((pRect->i16XMin)>(pRect->i16XMax))
		{
			uINT_16 bk=(pRect->i16XMax);
			(srecta.i16XMax)=(pRect->i16XMin);
			(srecta.i16XMin)=bk;
		}
		else
		{
			(srecta.i16XMax)=(pRect->i16XMax);
			(srecta.i16XMin)=(pRect->i16XMin);
		}

		if((pRect->i16YMin)>(pRect->i16YMax))
		{
			uINT_16 bk=(pRect->i16YMax);
			(srecta.i16YMax)=(pRect->i16YMin);
			(srecta.i16YMin)=bk;
		}
		else
		{
			(srecta.i16YMax)=(pRect->i16YMax);
			(srecta.i16YMin)=(pRect->i16YMin);
		}
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif

		return srecta;
}

void LCD_backlightInitRevAB()
{
#ifndef LCDBPV2RevC
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	//start
			//send address byte I2C_SLAVE_ADDRESS
			I2CMasterSlaveAddrSet( I2C_M_BASE, BACKLIGHT_I2C_ADDRESS, false);
			//send command byte
			I2CMasterDataPut( I2C_M_BASE, 0x10);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_START);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x00);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x00);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0xFF);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x44); //0x44
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x44);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x44);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x44);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x44);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0xFF);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			WaitI2CDone( I2C_M_BASE);

			I2CMasterDataPut( I2C_M_BASE, 0x0F);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
			WaitI2CDone( I2C_M_BASE);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
#endif
}

void setallscreen()
{
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	sRect.i16XMin = 0;
	sRect.i16YMin = 0;
	sRect.i16XMax = 319;
	sRect.i16YMax = 239;
#elif defined(MSP430G2)
	sRect.sXMin = 0;
	sRect.sYMin = 0;
	sRect.sXMax = 319;
	sRect.sYMax = 239;
#elif defined(__MSP430F5529__)

#endif
}

char LCDBPV2_Get1()
{

#ifdef LCDBPV2RevC
	char d0, d1, d2, d3;
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
				I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, true);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
				while(I2CMasterBusy(I2C_M_BASE));
				d0 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
				while(I2CMasterBusy(I2C_M_BASE));
				d1 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
				while(I2CMasterBusy(I2C_M_BASE));
				d2 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
				while(I2CMasterBusy(I2C_M_BASE));
				d3 = I2CMasterDataGet(I2C_M_BASE);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif

#endif
	return d0;
}

#if defined(TIVA)||defined(TIVACONNECTED) //defined(CC3200)||

void Message(const char *str)
{
	if(str != NULL)
	{
		while(*str!='\0')
		{
			UARTCharPut(UART0_BASE,*str++);
		}
	}
}
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif


char d0, d1, d2, d3;

void LCDBPV2_Get4()
{
//char dd0, dd1, dd2, dd3;
#ifdef LCDBPV2RevC

#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
				I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, true);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
				while(I2CMasterBusy(I2C_M_BASE));
				d0 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
				while(I2CMasterBusy(I2C_M_BASE));
				d1 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
				while(I2CMasterBusy(I2C_M_BASE));
				d2 = I2CMasterDataGet(I2C_M_BASE);

				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
				while(I2CMasterBusy(I2C_M_BASE));
				d3 = I2CMasterDataGet(I2C_M_BASE);

				/*
				d0=&dd0;
				d1=&dd1;
				d2=&dd2;
				d3=&dd3;
				*/
				/*
				*d0=(char *)malloc(dd0);
				*d1=(char *)malloc(dd1);
				*d2=(char *)malloc(dd2);
				*d3=(char *)malloc(dd3);
				*/
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif

#endif
}

char LCD_READINT()
{
#if defined(TIVA)||defined(TIVACONNECTED)||defined(CC3200)
	if (GPIOPinRead(LCD_INT_BASE,LCD_INT_PIN)==LCD_INT_PIN)
	{
		return 1;
	}
	else
	{
		return 0;
	}
#elif	MSP430G2
	return 0;
#elif defined(__MSP430F5529__)
	return 0;
#else
	return 0;
#endif

}
void LCD_sendoneI2C(char message)
{
#ifdef CC3200
unsigned char aucDataBuf;
aucDataBuf=(unsigned char)message; //0xB0
//I2C_IF_Read(ucDevAddr, aucDataBuf, ucLen);
I2C_IF_Write(TOUCH_I2C_ADDRESS, &aucDataBuf, 1, 0);
#elif defined(TIVA) || defined (TIVACONNECTED)
I2CMasterSlaveAddrSet( I2C_M_BASE, TOUCH_I2C_ADDRESS, false);
//send command byte
//****SETUP COMMAND: 1011+00+0(MAV filter)+1(1=90k, 0=50k IRQ PULLUP RESISTOR)
I2CMasterDataPut( I2C_M_BASE, message);
I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
WaitI2CDone( I2C_M_BASE);

#elif defined(__MSP430F5529__)

#endif
}

uint16_t LCD_READXY()
{
	unsigned char datah;
		unsigned char datal;
	#ifdef CC3200
		unsigned char aucRdDataBuf[12];
		I2C_IF_Read(TOUCH_I2C_ADDRESS, &aucRdDataBuf[0], 2);
		datah=aucRdDataBuf[0];
		datal=aucRdDataBuf[1];
	#endif
	#if defined(TIVA) || defined(TIVACONNECTED)
			//i2c read
			//send address byte I2C_SLAVE_ADDRESS
			I2CMasterSlaveAddrSet( I2C_M_BASE, TOUCH_I2C_ADDRESS, true);   // false = write, true = read
			//read data byte 1
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			while(I2CMasterBusy(I2C_M_BASE));
			while(I2CMasterBusy(I2C_M_BASE));
			datah = I2CMasterDataGet(I2C_M_BASE);
			//read data byte 2
			I2CMasterControl(I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //I2C_MASTER_CMD_BURST_RECEIVE_CONT
			while(I2CMasterBusy(I2C_M_BASE));
			while(I2CMasterBusy(I2C_M_BASE));
			datal = I2CMasterDataGet(I2C_M_BASE);


#elif defined(__MSP430F5529__)

	#endif
		return ((datal>>4)+(datah<<4)); //return ((datal/16)+(datah*16));
}
long ret[7];

void LCDBPV2_calibration2eeprom()
{
	#ifdef TIVA
	int32_t a;
	uint32_t c;
	a=ret[0];
	c=(int32_t)a;
	_SysCtlDelay(100);
	EEPROMProgram(&c, 0x0, sizeof(ret[0]));
	a=ret[1];
	c=(int32_t)a;
	EEPROMProgram(&c, 0x20, sizeof(ret[1]));
	a=ret[2];
	c=(int32_t)a;
	EEPROMProgram(&c, 0x40, sizeof(ret[2]));
	a=ret[3];
	c=(int32_t)a;
	EEPROMProgram(&c, 0x60, sizeof(ret[3]));
	a=ret[4];
	c=(int32_t)a;
	EEPROMProgram(&c, 0x80, sizeof(ret[4]));
	a=ret[5];
	c=(int32_t)a;
	EEPROMProgram(&c, 0xA0, sizeof(ret[5]));
	a=ret[6];
	c=(int32_t)a;
	EEPROMProgram(&c, 0xC0, sizeof(ret[6]));
	#endif
}
void LCDBPV2_eeprom2calibration()
{
	#ifdef TIVA
	uint32_t b[1];
	EEPROMRead(b, 0x0, sizeof(b));
	ret[0]=(int32_t)b[0];
	EEPROMRead(b, 0x20, sizeof(b));
	ret[1]=(int32_t)b[0];
	EEPROMRead(b, 0x40, sizeof(b));
	ret[2]=(int32_t)b[0];
	EEPROMRead(b, 0x60, sizeof(b));
	ret[3]=(int32_t)b[0];
	EEPROMRead(b, 0x80, sizeof(b));
	ret[4]=(int32_t)b[0];
	EEPROMRead(b, 0xA0, sizeof(b));
	ret[5]=(int32_t)b[0];
	EEPROMRead(b, 0xC0, sizeof(b));
	ret[6]=(int32_t)b[0];
	LCDBPV2_setparm(ret);
	#endif
}





#ifdef CC3200

#elif defined(MSP430G2)
int UART_PRINT(const char *pcFormat, ...)
{

}
#else

int UART_PRINT(const char *pcFormat, ...)
{
	/*
 int iRet = 0;
#ifndef NOTERM

  char *pcBuff, *pcTemp;
  int iSize = 256;

  va_list list;
  pcBuff = (char*)malloc(iSize);
  if(pcBuff == NULL)
  {
      return -1;
  }
  while(1)
  {
      va_start(list,pcFormat);
      iRet = vsnprintf(pcBuff,iSize,pcFormat,list);
      va_end(list);
      if(iRet > -1 && iRet < iSize)
      {
          break;
      }
      else
      {
          iSize*=2;
          if((pcTemp=realloc(pcBuff,iSize))==NULL)
          {
              Message("Could not reallocate memory\n\r");
              iRet = -1;
              break;
          }
          else
          {
              pcBuff=pcTemp;
          }

      }
  }
  Message(pcBuff);
  free(pcBuff);

#endif
  return iRet;
	 */
	return 0;
}

#endif

void _SysCtlDelay(unsigned long x)
{
	#if defined CC3200
	MAP_UtilsDelay((x));
	#elif defined(MSP430G2)

	#elif defined(__MSP430F5529__)

	#else
	SysCtlDelay(x);
	#endif
}


void SETUPIRQ()
{
	// ** Enable interrupt
	// ****************************************************************
	#if defined TIVA || defined TIVACONNECTED

		#if defined TIVACONNECTED
		MAP_SysCtlPeripheralEnable(LCD_INT_SCPE);
		MAP_GPIOPinTypeGPIOInput(LCD_INT_BASE, LCD_INT_PIN);
		#else
		//SysCtlPeripheralEnable(LCD_INT_SCPE); //En TIVA ya esta activo, es el mismo que I2C
		GPIOPinTypeGPIOInput(LCD_INT_BASE, LCD_INT_PIN);
		#endif



		//GPIOPadConfigSet(LCD_INT_BASE ,LCD_INT_PIN,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU); //pullup /!TEST

		GPIOIntDisable(LCD_INT_BASE, LCD_INT_PIN);
		GPIOIntClear(LCD_INT_BASE, LCD_INT_PIN);
		GPIOIntRegister(LCD_INT_BASE,touch_IntHandler);

		GPIOIntTypeSet(LCD_INT_BASE, LCD_INT_PIN, GPIO_BOTH_EDGES);//GPIO_FALLING_EDGE GPIO_LOW_LEVEL GPIO_DISCRETE_INT GPIO_RISING_EDGE GPIO_BOTH_EDGES GPIO_DISCRETE_INT

		GPIOIntEnable(LCD_INT_BASE, LCD_INT_PIN);

		//IntPrioritySet(INT_GPIO, 0x00);
		//IntEnable(LCD_INT_GPIO);
		//IntMasterEnable();
	#elif defined(CC3200)
		// *******Enable PIN_15 for INT
		// Enable Peripheral Clocks
		PRCMPeripheralClkEnable(LCD_INT_PERIPH, PRCM_RUN_MODE_CLK); //MAP_

		// Configure PIN_15 for GPIO Input
		PinTypeGPIO(LCD_INT_PIN_LP, PIN_MODE_0, false); //MAP_
		GPIODirModeSet(LCD_INT_GPIO_BASE, LCD_INT_GPIO_PIN, GPIO_DIR_MODE_IN); //MAP_

		//  D0	//PIN_15 (GPIO22) for GPIO Output

		// Register the power management ISR //
		//        register_isr(INT_PRCM, prcm_interrupt_handler, NULL);
		GPIOIntTypeSet(LCD_INT_GPIO_BASE, LCD_INT_GPIO_PIN, GPIO_BOTH_EDGES);//GPIO_FALLING_EDGE GPIO_LOW_LEVEL GPIO_DISCRETE_INT GPIO_RISING_EDGE GPIO_BOTH_EDGES
		IntRegister(LCD_INT_GPIO, touch_IntHandler); //gpiointhandler
		IntPrioritySet(LCD_INT_GPIO, INT_PRIORITY_LVL_1);

		GPIOIntClear(LCD_INT_GPIO_BASE, LCD_INT_GPIO_INT_PIN);
		GPIOIntEnable(LCD_INT_GPIO_BASE, LCD_INT_GPIO_INT_PIN);


		IntPendClear(LCD_INT_GPIO);
		IntEnable(LCD_INT_GPIO);
		IntMasterEnable();
	#elif defined(__MSP430F5529__)

	#endif
}

void SETUPGPIO()
{
	// **Configure EHA and INT pins
		// ****************************************************************
	#if defined(TIVA)

		#ifdef SERIAL

		//ACTIVATE PORT C
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
		//CONFIG PORT C
		GPIODirModeSet(GPIO_PORTC_BASE,GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(GPIO_PORTC_BASE,GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, GPIO_STRENGTH_8MA_SC,GPIO_PIN_TYPE_STD); //GPIO_STRENGTH_2MA

		// ****************************************************************
		// **SERIAL PINOUT**
		//
		// *Port C*
		//	CLK	C6
		//	CS	C7
		//	D0	C5
		//	D1	C4
		//	INT	E0
		//
		//*******************************************************************

		//DATA1 LOW
		HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = 0;
		//CLK LOW
		HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = 0;
		//DATA2 LOW
		HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = 0;
		//CS HIGH
		HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = LCD_SCS_PIN;
		#else
		// *******************************************************************
		// **PARALLEL PINOUT**
		//	HIGH BYTE		PORTA, PORTD
		//	LOW BYTE		PORTB
		//	RST				C4
		//	DC				C6
		//	WR				C5
		//	CS				C7
		// *******************************************************************

		// **********************************************************************************************************************
		SysCtlPeripheralEnable(LCD_DATAH47_PERIPH);
		SysCtlPeripheralEnable(LCD_DATAH03_PERIPH);
		SysCtlDelay(10);
		HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
		HWREG(GPIO_PORTD_BASE + GPIO_O_CR) = GPIO_PIN_7;

		GPIODirModeSet(LCD_DATAH47_BASE,LCD_DATAH4_PIN|LCD_DATAH5_PIN|LCD_DATAH6_PIN|LCD_DATAH7_PIN, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_DATAH47_BASE,LCD_DATAH4_PIN|LCD_DATAH5_PIN|LCD_DATAH6_PIN|LCD_DATAH7_PIN, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD);
		GPIODirModeSet(LCD_DATAH03_BASE,LCD_DATAH0_PIN|LCD_DATAH1_PIN|LCD_DATAH2_PIN|LCD_DATAH3_PIN, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_DATAH03_BASE,LCD_DATAH0_PIN|LCD_DATAH1_PIN|LCD_DATAH2_PIN|LCD_DATAH3_PIN, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD);

		SysCtlPeripheralEnable(LCD_DATAL_PERIPH);
		SysCtlPeripheralEnable(LCD_COMMAND_PERIPH);


		// Convert the PB7/TRST pin into a GPIO pin.  This requires the use of the
		// GPIO lock since changing the state of the pin is otherwise disabled.
		HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
		HWREG(GPIO_PORTB_BASE + GPIO_O_CR) = 0x80;

		// Configure the Port B pins while the lock is held.  The lock is only
		// needed to configure PB7 in this case but this saves us doing the
		// configuration twice, once for PB0-6 and again for PB7.
		GPIODirModeSet(LCD_DATAL_BASE, 0xff, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_DATAL_BASE, 0xff, GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD);

		HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
		HWREG(GPIO_PORTB_BASE + GPIO_O_CR) = 0x00;
		HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = 0;

		// Configure the pins that connect to the LCD as GPIO outputs.
		GPIODirModeSet(LCD_CS_BASE, LCD_CS_PIN, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_CS_BASE, LCD_CS_PIN, GPIO_STRENGTH_8MA,GPIO_PIN_TYPE_STD);

		GPIODirModeSet(LCD_DC_BASE, (LCD_DC_PIN |  LCD_WR_PIN),GPIO_DIR_MODE_OUT); // GPIODirModeSet(LCD_DC_BASE, (LCD_DC_PIN | LCD_RD_PIN | LCD_WR_PIN),GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_DC_BASE, (LCD_DC_PIN | LCD_WR_PIN),GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD); //GPIOPadConfigSet(LCD_DC_BASE, (LCD_DC_PIN | LCD_RD_PIN | LCD_WR_PIN),GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD);
		GPIODirModeSet(LCD_RST_BASE, LCD_RST_PIN, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(LCD_RST_BASE, LCD_RST_PIN, GPIO_STRENGTH_8MA,GPIO_PIN_TYPE_STD);
		// ***********************************************************************************************************************************

		// Set the LCD control pins to their default values.  This also asserts the LCD reset signal.
		HWREG(LCD_DATAH47_BASE + GPIO_O_DATA + ((LCD_DATAH4_PIN|LCD_DATAH5_PIN|LCD_DATAH6_PIN|LCD_DATAH7_PIN) << 2)) = 0;
		HWREG(LCD_DATAH03_BASE + GPIO_O_DATA + ((LCD_DATAH0_PIN|LCD_DATAH1_PIN|LCD_DATAH2_PIN|LCD_DATAH3_PIN) << 2)) = 0;
		GPIOPinWrite(LCD_DATAL_BASE, 0xff, 0x00);
		SysCtlDelay(100);

		GPIOPinWrite(LCD_DC_BASE, (LCD_DC_PIN | LCD_WR_PIN),( LCD_WR_PIN)); //GPIOPinWrite(LCD_DC_BASE, (LCD_DC_PIN | LCD_RD_PIN | LCD_WR_PIN),(LCD_RD_PIN | LCD_WR_PIN));
		GPIOPinWrite(LCD_RST_BASE, LCD_RST_PIN, 0x00);

		// Delay for 1ms.
		SysCtlDelay(TIME_MS);

		// Deassert the LCD reset signal.
		GPIOPinWrite(LCD_RST_BASE, LCD_RST_PIN, LCD_RST_PIN);

		// Delay for 1ms while the LCD comes out of reset.
		SysCtlDelay(TIME_MS);
		#endif
	#elif defined(TIVACONNECTED)
		//TOUCH interrupt pin
		SysCtlPeripheralEnable(LCD_INT_SCPE);
		GPIOPinTypeGPIOInput(LCD_INT_BASE, LCD_INT_PIN);
		GPIOPadConfigSet(LCD_INT_BASE, LCD_INT_PIN ,GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

		#ifdef SERIAL
		// ****************************************************************
		// **SERIAL PINOUT**
		//
		// *Port C*
		//	CLK	C6
		//	CS	C7
		//	D0	C5
		//	D1	C4
		//	INT	E0
		// *******************************************************************

		//ACTIVATE PORT C
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
		//CONFIG PORT C
		GPIODirModeSet(GPIO_PORTL_BASE,GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_DIR_MODE_OUT);
		GPIOPadConfigSet(GPIO_PORTL_BASE,GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_STRENGTH_8MA_SC,GPIO_PIN_TYPE_STD); //GPIO_STRENGTH_2MA

		//DATA1 LOW
		HWREG(LCD_SDATA0_BASE +  ((LCD_SDATA0_PIN) << 2)) = 0;
		//CLK LOW
		HWREG(LCD_SCLK_BASE +  ((LCD_SCLK_PIN) << 2)) = 0;
		//DATA2 LOW
		HWREG(LCD_SDATA1_BASE +  ((LCD_SDATA1_PIN) << 2)) = 0;
		//CS HIGH
		HWREG(LCD_SCS_BASE +  ((LCD_SCS_PIN) << 2)) = LCD_SCS_PIN;
		#endif

	#elif defined(CC3200)
		//**Enable peripheal
		MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);
		//MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK); //already called in pinmux.c PinMuxConfig()

		//**Configure pins
		// Configure PIN_45 (GPIO31) for GPIO Output
		MAP_PinTypeGPIO(PIN_45, PIN_MODE_0, false);
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_OUT);
		// Configure PIN_18 (GPIO28) for GPIO Output
		MAP_PinTypeGPIO(PIN_18, PIN_MODE_0, false);
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x10, GPIO_DIR_MODE_OUT);
		// Configure PIN_61 (GPIO6) for GPIO Output
		MAP_PinTypeGPIO(PIN_61, PIN_MODE_0, false);
		MAP_GPIODirModeSet(GPIOA0_BASE, 0x40, GPIO_DIR_MODE_OUT);
		// Configure PIN_62 (GPIO7) for GPIO Output
		MAP_PinTypeGPIO(PIN_62, PIN_MODE_0, false);
		MAP_GPIODirModeSet(GPIOA0_BASE, 0x80, GPIO_DIR_MODE_OUT);
		// Configure PIN_03 for GPIO Input
		//MAP_PinTypeGPIO(PIN_03, PIN_MODE_0, false);
		//MAP_GPIODirModeSet(GPIOA1_BASE, 0x10, GPIO_DIR_MODE_IN);
	#elif defined(__MSP430F5529__)

	#endif
}
void SETUPI2C()
{
	// **Configure I2C
	// ****************************************************************
	#if defined( TIVA)
		//	SCL	E4
		//	SDA	E5

		SysCtlPeripheralEnable(I2C_PORT_PERIPH);
		GPIOPinTypeI2CSCL(GPIO_PORTE_BASE, I2C_SCL); //I2C_BASE?
		GPIOPinTypeI2C(GPIO_PORTE_BASE, I2C_SDA);
		GPIOPinConfigure(I2C_GPIO_SCL);
		GPIOPinConfigure(I2C_GPIO_SDA);
		SysCtlPeripheralEnable(I2C_PERIPH);
		I2CMasterInitExpClk( I2C_M_BASE, SysCtlClockGet(), false);
	#elif defined (TIVACONNECTED)
		//	SCL	D0
		//	SDA	D1

		uint32_t g_ui32SysClock = MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);


		SysCtlPeripheralEnable(I2C_PORT_PERIPH);	//GPIO Periph
		SysCtlPeripheralEnable(I2C_PERIPH);		//I2C Periph

		GPIOPinConfigure(I2C_GPIO_SCL);
			GPIOPinConfigure(I2C_GPIO_SDA);

		GPIOPinTypeI2CSCL(I2C_BASE, I2C_SCL);
			GPIOPinTypeI2C(I2C_BASE, I2C_SDA);

		I2CMasterInitExpClk( I2C_M_BASE, SysCtlClockGet(), false); //SysCtlClockGet()
		//I2CMasterEnable(I2C_M_BASE);
	#elif defined (CC3200)
		//	SCL	E4
		//	SDA	E5
		//already initialized in main
	#ifdef CONFIGURE_I2C
		MAP_PRCMPeripheralClkEnable(I2C_PRCM, PRCM_RUN_MODE_CLK);
		MAP_PinTypeI2C(I2C_SCL, PIN_MODE_1);
		MAP_PinTypeI2C(I2C_SDA, PIN_MODE_1);

		//MAP_PRCMPeripheralClkEnable(I2C_PRCM, PRCM_RUN_MODE_CLK);
		//MAP_PRCMPeripheralReset(I2C_PRCM);
		//MAP_I2CMasterInitExpClk (I2C_PRCM, SYS_CLK, false);

		I2C_IF_Open(I2C_MASTER_MODE_FST); //Do all configuration
	#endif

	#elif defined(__MSP430F5529__)

	#endif
}



//revC
#ifdef LCDBPV2RevC
void send1i2c(char message)
{
#if defined(LCDBPV2RevC)
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, false);
	I2CMasterDataPut( I2C_M_BASE,message);
	I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
	WaitI2CDone( I2C_M_BASE);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
#endif
}

void send2i2c(char message1,char message2)
{
#if defined(LCDBPV2RevC)
#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, false);
	I2CMasterDataPut( I2C_M_BASE,message1);
	I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_START);
	WaitI2CDone( I2C_M_BASE);
	I2CMasterDataPut( I2C_M_BASE,message2);
	I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
	WaitI2CDone( I2C_M_BASE);
#elif defined(MSP430G2)

#elif defined(__MSP430F5529__)

#endif
#endif
}
#endif
