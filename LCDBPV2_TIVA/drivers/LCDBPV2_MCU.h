/*
 * LCDBPV2_MCU.h
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#ifndef LCDBPV2_MCU_H_
#define LCDBPV2_MCU_H_
#ifdef __cplusplus
extern "C"
{
#endif

//#include "LCDBPV2_setup.h"
//#if defined(TIVA)||defined(TIVACONNECTED)
//#include "inc/hw_types.h"
//#endif

#include "LCDBPV2_setup.h"
#include "grlib.h"

void LCD_D0_S();
void LCD_D0_R();

void LCD_D1_S();
void LCD_D1_R();

void LCD_CLK_S();
void LCD_CLK_R();

void LCD_CS_S();
void LCD_CS_R();

void LCD_GPIOINT_CLR();
void LCD_GPIOINT_DISABLE();
void LCD_GPIOINT_ENABLE();

void LCD_TIMERINT_CLR();
void LCD_TIMERINT_DISABLE();
void LCD_TIMERINT_ENABLE();

void LCD_TIMERINT_SETUP();

//#define INTSTAT //enable/disable interrupt blocking into LCDBPV2 LCD functions
extern char intstate;
extern void LCDBPV2_GPIOint_Clear();
extern void LCDBPV2_GPIOint_disable();
extern void LCDBPV2_GPIOint_enable();
extern void testLed(int led, int ms);

unsigned long WaitI2CDone( unsigned int long ulBase);

#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)

//typedef tRectangle mRectangle;
void setallscreen();


#elif defined(MSP430G2)
//typedef tRectangle mRectangle;

#define     LCD_DIR               P2DIR
#define     LCD_OUT               P2OUT

#define LCD_CS	BIT0
#define LCD_CLK	BIT1
#define LCD_D0	BIT2
#define LCD_D1	BIT3

#define LCD_SET(x)		LCD_OUT &= ~ x
#define LCD_RESET(x)	LCD_OUT |= x

#define PIN_SET(x,y)	y &= ~ x
#define PIN_RESET(x,y)	y |= x
#define PIN_ALTERN(x,y)	y ^= x

#define WIDGET_MSG_PAINT        0x00000001
#define WIDGET_MSG_PTR_DOWN     0x00000002
#define WIDGET_MSG_PTR_MOVE     0x00000003
#define WIDGET_MSG_PTR_UP       0x00000004
#define WIDGET_MSG_KEY_UP       0x00000005
#define WIDGET_MSG_KEY_DOWN     0x00000006
#define WIDGET_MSG_KEY_LEFT     0x00000007
#define WIDGET_MSG_KEY_RIGHT    0x00000008
#define WIDGET_MSG_KEY_SELECT   0x00000009

#endif

char LCDBPV2_Get1();

void _SysCtlDelay(unsigned long x);
int UART_PRINT(const char *pcFormat, ...);

/*
#if defined(CC3200)
extern void gpiointhandler();
#else
void Message(const char *str);
#endif
*/
void SETUPI2C();
void SETUPGPIO();
void SETUPIRQ();
void LCDBPV2_Get4();

void LCDBPV2_eeprom2calibration();
void LCDBPV2_calibration2eeprom();

//revC
//#if defined(LCDBPV2RevC)
void send2i2c(char message1,char message2);
void send1i2c(char message);
//#endif
char LCD_READINT();
void LCD_sendoneI2C(char message);
uint16_t LCD_READXY();

typedef char INT_8;
typedef short int INT_16;
typedef int INT_32;
typedef unsigned char uINT_8;
typedef unsigned short int uINT_16;
typedef unsigned int uINT_32;
//extern struct gRectangle;
typedef struct
{
    //
    //! The minimum X coordinate of the rectangle.
    //
	INT_16 i16XMin;

    //
    //! The minimum Y coordinate of the rectangle.
    //
	INT_16 i16YMin;

    //
    //! The maximum X coordinate of the rectangle.
    //
	INT_16 i16XMax;

    //
    //! The maximum Y coordinate of the rectangle.
    //
	INT_16 i16YMax;
}
gRectangle;

gRectangle LCD_prepareRect(const tRectangle *pRect);

extern char d0, d1, d2, d3;

#ifdef __cplusplus
}
#endif
#endif /* LCDBPV2_MCU_H_ */

