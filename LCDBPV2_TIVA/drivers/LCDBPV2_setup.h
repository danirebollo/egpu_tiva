/*
 * LCDBPV2_setup.h
 *
 *  Created on: 03/04/2015
 *      Author: Dani
 */

#ifndef LCDBPV2_SETUP_H_
#define LCDBPV2_SETUP_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

#include <stdio.h>
#include <time.h>

// *********************************************************************

// ** Microcontrollers: TIVA Launchpad, CC3200 Launchpad, TIVA Connected
// TIVA / TIVACONNECTED / CC3200 / MSP430G2 are defined in "predefined simbols section".
// If you like to define with this code please delete predefined symbol.

//#define TIVA
//#define TIVACONNECTED
//#define CC3200
//#define MSP430G2
// *********************************************************************

// ** Touch mode *******************************************************
//#define CONFIGURE_I2C //Only for CC3200. I2C driver is initialited in code because launchpad sensors. If I2C configuration is needed uncomment this line
// *********************************************************************

// ** Touch mode *******************************************************
#define TOUCH_SIMPLE			1
#define TOUCH_PERSISTENT_MOVE	2
#define TOUCH_PERSISTENT_DOWN	3
// *********************************************************************

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

//#ifdef __MSP430G2553__
//	#define MSP430G2
//#endif

#include "LCDBPV2_MCU.h"

//Leds blink when touch and slow touch speed (disable by default)
//#define TOUCH_DEBUG



#if defined(MSP430G2) /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "grlib.h"
//#include "widget.h"
//#include "canvas.h"
//#include "pushbutton.h"
#define SEC_TO_LOOP(x)        ((80000000/5)*x)
#define MSEC_TO_LOOP(x)        ((80000/5)*x)

#define TIME_MS		MSEC_TO_LOOP(1)
#define TIME_S		SEC_TO_LOOP(1)
#define TIMER_PERIOD TIME_S*1 //(PERIODIC_TEST_CYCLES/ 1000)*TIMER_PERIOD //ms

#elif defined(__MSP430F5529__) /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "grlib.h"
//#include "widget.h"
//#include "canvas.h"
//#include "pushbutton.h"
#define SEC_TO_LOOP(x)        ((80000000/5)*x)
#define MSEC_TO_LOOP(x)        ((80000/5)*x)

#define TIME_MS		MSEC_TO_LOOP(1)
#define TIME_S		SEC_TO_LOOP(1)
#define TIMER_PERIOD TIME_S*1 //(PERIODIC_TEST_CYCLES/ 1000)*TIMER_PERIOD //ms

#define WIDGET_ROOT             &g_sRoot
#define WIDGET_MSG_PAINT        0x00000001
#define WIDGET_MSG_PTR_DOWN     0x00000002
#define WIDGET_MSG_PTR_MOVE     0x00000003
#define WIDGET_MSG_PTR_UP       0x00000004
#define WIDGET_MSG_KEY_UP       0x00000005
#define WIDGET_MSG_KEY_DOWN     0x00000006
#define WIDGET_MSG_KEY_LEFT     0x00000007
#define WIDGET_MSG_KEY_RIGHT    0x00000008
#define WIDGET_MSG_KEY_SELECT   0x00000009

#elif defined(CC3200) /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <stdbool.h>

#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_gpio.h"

#include "grlib.h"
#include "widget.h"
#include "canvas.h"
#include "pushbutton.h"
#include "interrupt.h"
#include "pin.h"
#include "gpio.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include "utils.h"
#include "timer_if.h"
#include "timer.h"
#include "common.h"
#include "uart_if.h"
#include "driverlib/uart.h"
#include "i2c.h"
#include "i2c_if.h"
#include "utils/ustdlib.h"

#define SEC_TO_LOOP(x)        ((80000000/5)*x)
#define MSEC_TO_LOOP(x)        ((80000/5)*x)

#define TIME_MS		MSEC_TO_LOOP(1)
#define TIME_S		SEC_TO_LOOP(1)
#define TIMER_PERIOD TIME_S*1 //(PERIODIC_TEST_CYCLES/ 1000)*TIMER_PERIOD //ms

#else /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/eeprom.h"
#include "driverlib/timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/udma.h"
#include "driverlib/uart.h"
//#include "driverlib/adc.h"

#include "grlib/grlib.h"
#include "grlib/widget.h"

#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
//#include "inc/hw_timer.h"
//#include "inc/hw_adc.h"

#include "utils/ustdlib.h"

#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/pushbutton.h"

#include "utils/uartstdio.h"

#if defined(TIVA)
#define TIME_MS		(ROM_SysCtlClockGet()/(3*1000))
#define TIMER_PERIOD TIME_MS*100//SysCtlClockGet()/1

#elif defined (TIVACONNECTED)
#define TIME_MS		(SysCtlClockGet()/(3*100))
#define TIMER_PERIOD TIME_MS*300
#endif

#endif

//! Serial mode and Microcontroller are "predefined symbols" for more comfort:
//! Go to "project", "propierties", "Build", "ARM Compiler", "Advanced options", "Predefined Symbols", "Pre-define NAME (--define, -D)".
//! Add Microcontroller type tag (ex: for Tiva launchpad type "TIVA" without quotes)
//! Add Comunication type tag (ex: for Tiva Connected only Serial mode is allowed, so type "SERIAL" without quotes)

// ** Communication mode: SERIAL or PARALLEL (nothing defined) *********
#define SERIAL
// *********************************************************************

	// ** LCDBPV2RevC I2C Features *********
#define LCDBPV2RevC
#define LCDBPV2RevC_EHAPLUS

#define LCDBPV2RevC_INTEGRATEDTIMER

#define LCDBPV3ADDR 0x4C



#define TOUCH_UP false
#define TOUCH_DOWN true

// 0: 	empty
// 4:	WIDGET_MSG_PTR_UP
// 2:	WIDGET_MSG_PTR_DOWN
// 3:	WIDGET_MSG_PTR_MOVE
//#define WIDGET_MSG_PTR_DOWN     0x00000002
//#define WIDGET_MSG_PTR_MOVE     0x00000003
//#define WIDGET_MSG_PTR_UP       0x00000004

extern char widget_touch_state;




#define TOUCH_I2C_ADDRESS 0x48
#define BACKLIGHT_I2C_ADDRESS 0x45

#define TOUCH_THRESHOLD 10000
#define	LCD_MAX_X	320
#define LCD_MAX_Y	240
// *********************************************************************************





#ifdef TIVA

#define RED_LED   	GPIO_PIN_1
#define BLUE_LED  	GPIO_PIN_2
#define GREEN_LED 	GPIO_PIN_3
#define LED_PORT	GPIO_PORTF_BASE



	//#include "inc/tm4c123gh6pm.h"

	#define I2C_M_BASE 			I2C2_BASE
	#define I2C_BASE 			GPIO_PORTE_BASE
	#define I2C_PERIPH 			SYSCTL_PERIPH_I2C2
	#define I2C_PORT_PERIPH		SYSCTL_PERIPH_GPIOE
	#define I2C_GPIO_SCL		GPIO_PE4_I2C2SCL
	#define I2C_GPIO_SDA		GPIO_PE5_I2C2SDA
	#define I2C_SCL				GPIO_PIN_4
	#define I2C_SDA				GPIO_PIN_5
	#define LCD_INT_SCPE		SYSCTL_PERIPH_GPIOE
	#define	LCD_INT_GPIO		INT_GPIOE
	#define LCD_INT_PIN			GPIO_PIN_0
	#define LCD_INT_BASE		GPIO_PORTE_BASE
	#define LCD_TIMER0_BASE		TIMER0_BASE
	#define LCD_TIMER0_PERIPH	SYSCTL_PERIPH_TIMER0
	#define LCD_TIMER			TIMER_A
	#define LCD_TIMER0			INT_TIMER0A
	#define LCD_TIMER_TIMEOUT	TIMER_TIMA_TIMEOUT

	#ifdef SERIAL
		//*******************************************************************
		//	I2C_SCL	E4
		//	I2C_SDA	E5
		//	CLK	C6
		//	CS	C7
		//	D0	C5
		//	D1	C4
		//	INT	E0
		//*******************************************************************

		#define LCD_SCS_BASE            GPIO_PORTC_BASE
		#define LCD_SCS_PIN             GPIO_PIN_7

		#define LCD_SDATA0_BASE            GPIO_PORTC_BASE
		#define LCD_SDATA0_PIN             GPIO_PIN_5

		#define LCD_SDATA1_BASE            GPIO_PORTC_BASE
		#define LCD_SDATA1_PIN             GPIO_PIN_4

		#define LCD_SCLK_BASE            GPIO_PORTC_BASE
		#define LCD_SCLK_PIN             GPIO_PIN_6
	#else
		//*******************************************************************
		//	I2C_SCL	E4
		//	I2C_SDA	E5
		//	HIGH BYTE		PORT A, D
		//	LOW BYTE		PORT B
		//	RST				C4
		//	DC				C6
		//	WR				C5
		//	CS				C7
		//*******************************************************************
		#define LCD_DATAH03_PERIPH      SYSCTL_PERIPH_GPIOD
		#define LCD_DATAH03_BASE        GPIO_PORTD_BASE
		#define LCD_DATAH47_PERIPH      SYSCTL_PERIPH_GPIOA
		#define LCD_DATAH47_BASE        GPIO_PORTA_BASE
		#define LCD_DATAH0_PIN          GPIO_PIN_0
		#define LCD_DATAH1_PIN          GPIO_PIN_1
		#define LCD_DATAH2_PIN          GPIO_PIN_2
		#define LCD_DATAH3_PIN          GPIO_PIN_3
		#define LCD_DATAH4_PIN          GPIO_PIN_4
		#define LCD_DATAH5_PIN          GPIO_PIN_5
		#define LCD_DATAH6_PIN          GPIO_PIN_6
		#define LCD_DATAH7_PIN          GPIO_PIN_7

		#define LCD_DATAL_PERIPH        SYSCTL_PERIPH_GPIOB
		#define LCD_DATAL_BASE          GPIO_PORTB_BASE

		#define LCD_COMMAND_PERIPH      SYSCTL_PERIPH_GPIOC

		#define LCD_RST_BASE            GPIO_PORTC_BASE
		#define LCD_RST_PIN             GPIO_PIN_4

		#define LCD_DC_BASE             GPIO_PORTC_BASE
		#define LCD_DC_PIN              GPIO_PIN_6

		#define LCD_WR_BASE             GPIO_PORTC_BASE
		#define LCD_WR_PIN              GPIO_PIN_5

		#define LCD_CS_BASE             GPIO_PORTC_BASE
		#define LCD_CS_PIN              GPIO_PIN_7
	#endif
#endif

#ifdef	CC3200

#define SYS_CLK                 80000000

	#define I2C_PRCM			PRCM_I2CA0
	#define I2C_BASE 			GPIO_PORTE_BASE
	#define I2C_PERIPH 			SYSCTL_PERIPH_I2C2
	#define I2C_PORT_PERIPH		SYSCTL_PERIPH_GPIOE
	#define I2C_GPIO_SCL		GPIO_PE4_I2C2SCL
	#define I2C_GPIO_SDA		GPIO_PE5_I2C2SDA
	#define I2C_SCL				PIN_01
	#define I2C_SDA				PIN_02
	#define I2C_M_BASE  		I2CA0_BASE //PRCM_I2CA0


//#define LCD_INT_SCPE		SYSCTL_PERIPH_GPIOG
	#define LCD_INT_PERIPH			PRCM_GPIOA0 //A2
	#define LCD_INT_GPIO			INT_GPIOA0
	#define LCD_INT_GPIO_BASE		GPIOA0_BASE

	#define LCD_INT_GPIO_INT_PIN	GPIO_INT_PIN_4 //6
	#define LCD_INT_GPIO_PIN		GPIO_PIN_4

	#define LCD_INT_PIN_LP				PIN_59

	#define LCD_INT_BASE		LCD_INT_GPIO_BASE
	#define LCD_INT_PIN				LCD_INT_GPIO_INT_PIN


	#define LCD_TIMER0_BASE		TIMERA0_BASE
	#define LCD_TIMER0_PERIPH	SYSCTL_PERIPH_TIMER0
	#define LCD_TIMER			TIMER_A
	#define LCD_TIMER0			INT_TIMER0A
	#define LCD_TIMER_TIMEOUT	TIMER_TIMA_TIMEOUT

	#ifdef SERIAL
		//*******************************************************************
		//	I2C_SCL	A1	2
		//	I2C_SDA	A1	3
		//	CLK		A0	6
		//	CS		A0 	7
		//	D0		A3 	7
		//	D1		A3 	4
		//	INT	E0
		//*******************************************************************

		#define LCD_SCS_BASE            GPIOA0_BASE
		#define LCD_SCS_PIN             GPIO_PIN_7

		#define LCD_SDATA0_BASE            GPIOA3_BASE
		#define LCD_SDATA0_PIN             GPIO_PIN_7

		#define LCD_SDATA1_BASE            GPIOA3_BASE
		#define LCD_SDATA1_PIN             GPIO_PIN_4

		#define LCD_SCLK_BASE            GPIOA0_BASE
		#define LCD_SCLK_PIN             GPIO_PIN_6

extern void gpiointhandler();
void TimerBaseIntHandler(void);



	#else
	#endif

#endif

#ifdef	TIVACONNECTED

#define LED_1   	1
#define LED_2  		2
#define LED_3	 	3
#define LED_4	 	4
#define LED_1_PIN   	GPIO_PIN_1
#define LED_2_PIN  		GPIO_PIN_0
#define LED_3_PIN	 	GPIO_PIN_4
#define LED_4_PIN	 	GPIO_PIN_0
#define LED_PORT_12	GPIO_PORTN_BASE
#define LED_PORT_34	GPIO_PORTF_BASE

	#define I2C_BASE 			GPIO_PORTD_BASE
	#define I2C_PERIPH 			SYSCTL_PERIPH_I2C7
	#define I2C_PORT_PERIPH		SYSCTL_PERIPH_GPIOD
	#define I2C_GPIO_SCL		GPIO_PD0_I2C7SCL
	#define I2C_GPIO_SDA		GPIO_PD1_I2C7SDA
	#define I2C_SCL				GPIO_PIN_0
	#define I2C_SDA				GPIO_PIN_1
	#define I2C_M_BASE  		I2C7_BASE

	#define LCD_INT_SCPE		SYSCTL_PERIPH_GPIOG
	#define LCD_INT_PIN			GPIO_PIN_0
	#define LCD_INT_BASE		GPIO_PORTG_BASE
	#define	LCD_INT_GPIO		INT_GPIOG
	#define LCD_TIMER0_BASE		TIMER0_BASE
	#define LCD_TIMER0_PERIPH	SYSCTL_PERIPH_TIMER0
	#define LCD_TIMER			TIMER_A
	#define LCD_TIMER0			INT_TIMER0A
	#define LCD_TIMER_TIMEOUT	TIMER_TIMA_TIMEOUT

	#ifdef SERIAL
		//*******************************************************************
		//	I2C_SCL	D0
		//	I2C_SDA	D1
		//	CLK	L1
		//	CS	L0
		//	D0	L2
		//	D1	L3
		//	INT	G0
		//*******************************************************************

		#define LCD_SCS_BASE            GPIO_PORTL_BASE
		#define LCD_SCS_PIN             GPIO_PIN_0

		#define LCD_SDATA0_BASE            GPIO_PORTL_BASE
		#define LCD_SDATA0_PIN             GPIO_PIN_2

		#define LCD_SDATA1_BASE            GPIO_PORTL_BASE
		#define LCD_SDATA1_PIN             GPIO_PIN_3

		#define LCD_SCLK_BASE            GPIO_PORTL_BASE
		#define LCD_SCLK_PIN             GPIO_PIN_1
	#else
	#endif

#endif

#ifdef	MSP430G2

/*
#define LED_1   	1
#define LED_2  		2
#define LED_3	 	3
#define LED_4	 	4
#define LED_1_PIN   	GPIO_PIN_1
#define LED_2_PIN  		GPIO_PIN_0
#define LED_3_PIN	 	GPIO_PIN_4
#define LED_4_PIN	 	GPIO_PIN_0
#define LED_PORT_12	GPIO_PORTN_BASE
#define LED_PORT_34	GPIO_PORTF_BASE
*/
	#define I2C_BASE 			GPIO_PORTD_BASE
	#define I2C_PERIPH 			SYSCTL_PERIPH_I2C7
	#define I2C_PORT_PERIPH		SYSCTL_PERIPH_GPIOD
	#define I2C_GPIO_SCL		GPIO_PD0_I2C7SCL
	#define I2C_GPIO_SDA		GPIO_PD1_I2C7SDA
	#define I2C_SCL				GPIO_PIN_0
	#define I2C_SDA				GPIO_PIN_1
	#define I2C_M_BASE  		I2C7_BASE

	#define LCD_INT_SCPE		SYSCTL_PERIPH_GPIOG
	#define LCD_INT_PIN			GPIO_PIN_0
	#define LCD_INT_BASE		GPIO_PORTG_BASE
	#define	LCD_INT_GPIO		INT_GPIOG
	#define LCD_TIMER0_BASE		TIMER0_BASE
	#define LCD_TIMER0_PERIPH	SYSCTL_PERIPH_TIMER0
	#define LCD_TIMER			TIMER_A
	#define LCD_TIMER0			INT_TIMER0A
	#define LCD_TIMER_TIMEOUT	TIMER_TIMA_TIMEOUT

	#ifdef SERIAL
		//*******************************************************************
		//	I2C_SCL	D0
		//	I2C_SDA	D1
		//	CLK	L1
		//	CS	L0
		//	D0	L2
		//	D1	L3
		//	INT	G0
		//*******************************************************************

		#define LCD_SCS_BASE            GPIO_PORTL_BASE
		#define LCD_SCS_PIN             GPIO_PIN_0

		#define LCD_SDATA0_BASE            GPIO_PORTL_BASE
		#define LCD_SDATA0_PIN             GPIO_PIN_2

		#define LCD_SDATA1_BASE            GPIO_PORTL_BASE
		#define LCD_SDATA1_PIN             GPIO_PIN_3

		#define LCD_SCLK_BASE            GPIO_PORTL_BASE
		#define LCD_SCLK_PIN             GPIO_PIN_1
	#else
	#endif

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
void LCDBPV2_setup(char touchm);

extern tContext sContext;
extern tRectangle sRect;

void ClrScreen(int color);
extern void makeCal();
extern void Timer0IntHandler();
extern void disableINT();
extern void enableINT();



void _UART_I_PRINT(int pcFormat);
void _UART_I_PRINTLN(int pcFormat);
void _UART_C_PRINT(const char *pcFormat);
void _UART_C_PRINTLN(const char *pcFormat);
void _UART_H_PRINT(char pcFormat);
void _UART_H_PRINTLN(char pcFormat);
void _UART_L_PRINT(unsigned long x);
void _UART_L_PRINTLN(unsigned long x);



//LCDBPV2revC
void LCDBPV2_VibratorPulse();
void LCDBPV2_DefaultValues();
void LCDBPV2_BacklightState(char state);
void LCDBPV2_BacklightPulse();
void LCDBPV2_BacklightPulsePeriod(char state);
void LCDBPV2_BacklightPWMFreq(char state);
void LCDBPV2_BacklightPWMDuty(char state);
void LCDBPV2_VibratorState(char state);
void LCDBPV2_VibratorPulsePeriod(char state);
void LCDBPV2_VibratorPWMFreq(char state);
void LCDBPV2_VibratorPWMDuty(char state);
void LCDBPV2_IRQFilterPeriod(char state);
void LCDBPV2_IRQPersistenceState(char state);
void LCDBPV2_IRQPersistencePeriod(char state);
void LCDBPV2_LCDEnable(char state);
void LCDBPV2_LCDReset(char state);
void LCDBPV2_FIFOI2CState(char state);
void LCDBPV2_FIFOLCDState(char state);
char LCDBPV2_GetFIFOLCDFullFlag();
void LCDBPV2_ResetFIFOLCDState();
void LCDBPV2_SetI2CWatchdogCounter(char counter);
void LCDBPV2_EnableI2CWatchdogCounter(char state);


void RevC_GetTouchRAWZ1Z2();
void RevC_ResetFIFOFull();
void RevC_GetFIFOFull();
void RevC_GetTouchRAWXY();
void RevC_EnableI2CWatchdogCounter(char state);
void RevC_SetI2CWatchdogCounter(char state);
void RevC_SetTOUCHFlags(char state);
void RevC_Reset2Defaults();
void RevC_FIFOLCDEnable(char state);
void RevC_FIFOI2CEnable(char state);
void RevC_LCDReset(char state);
void RevC_LCDEnable(char state);
void RevC_IRQPersistencePeriod(char state);
void RevC_IRQPersistence(char state);
void RevC_IRQFilterPeriod(char state);
void RevC_ERMPWMDuty(char state);
void RevC_ERMPWMFreq(char state);
void RevC_ERMPulsePeriod(char state);
void RevC_ERMPulse();
void RevC_ERMForceEnable(char state);
void RevC_BKPWMDuty(char state);
void RevC_BKPWMFreq(char state);
void RevC_BKPulsePeriod(char state);
void RevC_BKPulse();
void RevC_BKForceEnable(char state);
/////////////////////////////////////////////////////


extern void touch_IntHandler(void);
extern void Timer0IntHandler(void);


extern void LCDBPV2_SetTouchPeriod(unsigned long period);
extern char intstate;
extern unsigned long touch_period;

#endif /* LCDBPV2_SETUP_H_ */
