/*
 * LCDBPV2_touch.h
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#ifndef LCDBPV2_TOUCH_H_
#define LCDBPV2_TOUCH_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include "LCDBPV2_setup.h"

#ifdef CC3200
#include "grlib.h"
#include "widget.h"
#include "i2c_if.h"
#else

#endif

//extern void LCDBPV2_touch_IntHandler(void);
extern void touch_IntHandler(void);
void gettouch();
void LCDBPV2_touch_Debouncer(void);
void LCDBPV2_touch_Init()  ;

//void LCDBPV2_touch_CallbackSet(long (*pfnCallback)(unsigned long ulMessage, long lX, long lY));
void LCDBPV2_TouchScreenCallbackSet(int32_t (*pfnCallback)(uint32_t ulMessage, int32_t lX, int32_t lY));

void LCDBPV2_touch_CalibrationPoint(unsigned short sPointX, unsigned short sPointY, unsigned long ulPointIndex);
long* LCDBPV2_touch_Calibrate(void);
//stores X,Y,Z1,Z2 and Rtouch data.
extern uint16_t tX,tY;
uint16_t tZ1,tZ2,tRtouch; //int cambiar a 16bit uint16_t //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

typedef void (*functiontype2)();
void addevent(functiontype2 event1);
//send a command to touch driver and receives and acknowledge (x,y,z coordenates)
uint16_t LCDBPV2_touch_receiveI2C(char op);
void touchint(int status );
void touchprocess(char state);
char touch_state();
//flag that indicates if data needs to be calibrated (0) or not(1)
char flcal;

void TouchScreenCalibrationPoint(uint16_t sPointX, uint16_t sPointY, uint16_t ulPointIndex);
void nothing(uint16_t sPointX, uint16_t sPointY, uint16_t ulPointIndex);

long* LCDBPV2_TouchScreenCalibrate(void);
void LCDBPV2_calcParam();
//flag to indicate I2C error (timeout). Timer 1 needs to be enabled. [DISABLED]
char I2Cwd;
char laststate;
extern char bussyint;
long ret[7];
void LCDBPV2_setparm(long* parm);

long* LCDBPV2_loadparm();
void LCDBPV2_touchMode(char _touchmode);

extern char d0, d1, d2, d3;

//Tiva Launchpad only
void LCDBPV2_calibration2eeprom();
void LCDBPV2_eeprom2calibration();
void TouchScreenCallbackSet(int32_t (*pfnCallback)(uint32_t ui32Message, int32_t i32X, int32_t i32Y));

#ifdef __cplusplus
}
#endif
#endif /* LCDBPV2_TOUCH_H_ */

