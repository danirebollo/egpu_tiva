/*
 * LCDBPV2_setup.c
 *
 *  Created on: 03/04/2015
 *      Author: Dani
 */

#include "LCDBPV2_setup.h"

#include "LCDBP320x240x16_SSD1289.h"
#include "LCDBPV2_backlight.h"
#include "LCDBPV2_touch.h"


extern void touch_IntHandler(void);
char widget_touch_state;
tContext sContext;
tRectangle sRect;
//char handledinterrupt;
char intstate;
unsigned long touch_period;
void disableINT()
{

}
void enableINT()
{

}

void LCDBPV2_SetTouchPeriod(unsigned long period)
{
//	#if defined(LCDBPV2RevC_INTEGRATEDTIMER)
//		LCDBPV2_IRQPersistencePeriod((char)period);
//	#else
//		touch_period=period;
//	#endif
}

void LCDBPV2_setup(char touchm)
{
	//**** Enable MCU peripheals
	SETUPI2C();
	SETUPGPIO();
	/*   //Enable Pin A2 (buzzer)
	    GPIOPinTypeGPIOOutput(LCD_DATAH47_BASE, LCD_DATAH2_PIN);
	    //Enable Pin F4 (vibrator)
	    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_4);*/
	_SysCtlDelay(100);

	intstate=0;

	#if defined LCDBPV2RevC
	#if defined LCDBPV2RevC_EHAPLUS
	RevC_FIFOLCDEnable(1);
	#else
	RevC_FIFOLCDEnable(0);
	#endif
	#endif

	touch_period=TIMER_PERIOD;

#if defined(LCDBPV2RevC)
	RevC_IRQPersistence(0);
#endif

#if defined(LCDBPV2RevC_INTEGRATEDTIMER)
	LCDBPV2_IRQPersistencePeriod(2);
#else
	//touch_period=period;
#endif

	_SysCtlDelay(100);

	LCDBP320x240x16_SSD1289Init();
	LCDBPV2_BacklightInit();
	LCDBPV2_touch_Init();
	LCDBPV2_loadparm();

	if((touchm==TOUCH_SIMPLE)||(touchm==TOUCH_PERSISTENT_MOVE)||(touchm==TOUCH_PERSISTENT_DOWN))
		{
			//#if (defined(LCDBPV2RevC_INTEGRATEDTIMER))
			//	if((touchm==TOUCH_PERSISTENT_MOVE))
			//	{
			//		touchm=TOUCH_PERSISTENT_DOWN;
			//	}
			//#endif
			LCDBPV2_touchMode(touchm);
		}
		_SysCtlDelay(TIME_MS);
		/////////////////////////////////////////////////////////////////////////
		GrContextInit(&sContext, &g_sLCDBP320x240x16_SSD1289);
		widget_touch_state=0;

	// ** Enable timer
	// ****************************************************************
//#if (defined(LCDBPV2RevC_INTEGRATEDTIMER) && defined(LCDBPV2RevC))

//	RevC_IRQPersistence(1);
//#else

		LCD_TIMERINT_SETUP();
		_SysCtlDelay(TIME_MS);
//#endif

	SETUPIRQ();

	intstate=1;

	//Not obligatory but recommended
	//LCDBPV2_DefaultValues();
	LCDBPV2_IRQFilterPeriod(0);
	LCDBPV2_SetI2CWatchdogCounter(2);
	LCDBPV2_EnableI2CWatchdogCounter(1);

	_UART_C_PRINTLN("SETUP DONE");
}

void Timer0IntHandler(void)
{
	//GPIOIntClear(LCD_TIMER0_BASE, LCD_TIMER_TIMEOUT); //!
	//GPIOIntDisable(LCD_INT_BASE, GPIO_BOTH_EDGES);  //!
	_SysCtlDelay(100);
	LCD_TIMERINT_CLR();
	_SysCtlDelay(100);
	LCD_TIMERINT_DISABLE();

	intstate=0;
	_SysCtlDelay(100);

	//testLed(LED_4, 500);


	_SysCtlDelay(100);
	if (touch_state()==TOUCH_DOWN)
	{
#if defined (TOUCH_DEBUG)
		testLed(BLUE_LED,100);
#endif

		_SysCtlDelay(100);
		touchprocess(TOUCH_DOWN);
		_SysCtlDelay(1000);

		LCD_TIMERINT_CLR();
		LCD_TIMERINT_ENABLE();

		_SysCtlDelay(TIME_MS);
	}
	else
	{
#if defined (TOUCH_DEBUG)
		testLed(RED_LED,100);
#endif
		_SysCtlDelay(100);
		touchprocess(TOUCH_UP);
		_SysCtlDelay(1000);

		_SysCtlDelay(TIME_MS);
		LCD_GPIOINT_CLR();
	_SysCtlDelay(TIME_MS);

	LCD_GPIOINT_ENABLE();

		intstate=1;
	}
}

void makeCal()
{
	// Paint touch calibration targets and collect calibration data
	GrContextForegroundSet(&sContext,ClrWhite);
	GrContextBackgroundSet(&sContext,ClrBlack);
	GrContextFontSet(&sContext,&g_sFontCm20);

	//StringDraw("Touch center of circles to calibrate", -1, 0, 0, 1);
	GrCircleDraw(&sContext,32, 24, 10);
	_SysCtlDelay(TIME_MS*500);
	GrContextForegroundSet(&sContext,ClrYellow);
	GrCircleFill(&sContext,32, 24, 8);
	TouchScreenCalibrationPoint(32, 24, 0);

	GrContextForegroundSet(&sContext,ClrWhite);
	GrCircleDraw(&sContext,280, 200, 10);
	_SysCtlDelay(TIME_MS*500);
	GrContextForegroundSet(&sContext,ClrYellow);
	GrCircleFill(&sContext,280, 200, 8);
	TouchScreenCalibrationPoint(280, 200, 1);

	GrContextForegroundSet(&sContext,ClrWhite);
	GrCircleDraw(&sContext,200, 40, 10);
	_SysCtlDelay(TIME_MS*500);
	GrContextForegroundSet(&sContext,ClrYellow);
	GrCircleFill(&sContext,200, 40, 8);
	GrContextForegroundSet(&sContext,ClrWhite);
	TouchScreenCalibrationPoint(200, 40, 2);
	_SysCtlDelay(100);
	ClrScreen(ClrBlack);
	_SysCtlDelay(100);
	// Calculate and set calibration matrix
	long* plCalibrationMatrix = LCDBPV2_TouchScreenCalibrate();

	GrContextForegroundSet(&sContext,ClrYellow);
	GrContextBackgroundSet(&sContext,ClrBlack);

	char pcStringBuf[20];
	usprintf(pcStringBuf, "A %d", plCalibrationMatrix[0]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 20, 1);
	usprintf(pcStringBuf, "B %d", plCalibrationMatrix[1]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 40, 1);
	usprintf(pcStringBuf, "C %d", plCalibrationMatrix[2]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 60, 1);
	usprintf(pcStringBuf, "D %d", plCalibrationMatrix[3]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 80, 1);
	usprintf(pcStringBuf, "E %d", plCalibrationMatrix[4]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 100, 1);
	usprintf(pcStringBuf, "F %d", plCalibrationMatrix[5]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 120, 1);
	usprintf(pcStringBuf, "Div %d", plCalibrationMatrix[6]);
	GrStringDraw(&sContext,pcStringBuf, -1, 10, 140, 1);

	GrContextForegroundSet(&sContext,ClrWhite);
	GrContextBackgroundSet(&sContext,ClrBlack);
	GrContextFontSet(&sContext,&g_sFontCm20b);

	//usprintf(pcStringBuf, "Please put this coefficients into function LCDBPV2_loadparm() drivers/LCDBPV2_touch.c");
	GrStringDraw(&sContext,"Please put this coefficients into ", -1, 10, 160, 1);
	_SysCtlDelay(100);
	GrStringDraw(&sContext,"LCDBPV2-loadparm() function", -1, 10, 180, 1);
	_SysCtlDelay(100);
	GrStringDraw(&sContext,"at drivers/LCDBPV2-touch.c", -1, 10, 200, 1);
	_SysCtlDelay(102);

	TouchScreenCalibrationPoint(0,0,0);	// wait for dummy touch
}
void ClrScreen(int color)
{
	setallscreen();

	GrContextForegroundSet(&sContext, color);
	GrRectFill(&sContext, &sRect);
	GrFlush(&sContext);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCDBPV2_VibratorPulse()
{
	RevC_ERMPulse();
}
void LCDBPV2_VibratorState(char state)
{
	RevC_BKForceEnable(state);
}

void LCDBPV2_VibratorPulsePeriod(char state)
{
	RevC_BKPulsePeriod(state);
}

void LCDBPV2_VibratorPWMFreq(char state)
{
	RevC_ERMPWMFreq(state);
}

void LCDBPV2_VibratorPWMDuty(char state)
{
	RevC_ERMPWMDuty(state);
}

void LCDBPV2_DefaultValues()
{
	RevC_Reset2Defaults();

}

void LCDBPV2_IRQFilterPeriod(char state)
{
	RevC_IRQFilterPeriod(state);
}

void LCDBPV2_IRQPersistenceState(char state)
{
	RevC_IRQPersistence(state);
}

void LCDBPV2_IRQPersistencePeriod(char state)
{
	RevC_IRQPersistencePeriod(state);
}

void LCDBPV2_LCDEnable(char state)
{
	RevC_LCDEnable(state);
}

void LCDBPV2_LCDReset(char state)
{
	if (state==0x00)
		RevC_LCDReset(0x01);
	else
		RevC_LCDReset(0x00);
}

void LCDBPV2_FIFOI2CState(char state)
{
	RevC_FIFOI2CEnable(state);
}

void LCDBPV2_FIFOLCDState(char state)
{
	RevC_FIFOLCDEnable(state);
}



char LCDBPV2_GetFIFOLCDFullFlag()
{
	char d0;
	RevC_GetFIFOFull();
	d0=LCDBPV2_Get1();
	return d0;
}

void LCDBPV2_ResetFIFOLCDState()
{
	RevC_ResetFIFOFull();
}

void LCDBPV2_SetI2CWatchdogCounter(char counter)
{
	RevC_SetI2CWatchdogCounter(counter);
}

void LCDBPV2_EnableI2CWatchdogCounter(char state)
{
	RevC_EnableI2CWatchdogCounter(state);
}



#ifdef LCDBPV2RevC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BK

void RevC_BKForceEnable(char state)
{
	send2i2c(0x00,state);
}

void RevC_BKPulse()
{
	send1i2c(0x01);
}

void RevC_BKPulsePeriod(char state)
{
	send2i2c(0x02,state);
}

void RevC_BKPWMFreq(char state)
{
	send2i2c(0x03,state);
}

void RevC_BKPWMDuty(char state)
{
	send2i2c(0x04,state);
}
//ERM

void RevC_ERMForceEnable(char state)
{
	send2i2c(0x05,state);
}

void RevC_ERMPulse()
{
	send1i2c(0x06);
}

void RevC_ERMPulsePeriod(char state)
{
	send2i2c(0x07,state);
}

void RevC_ERMPWMFreq(char state)
{
	send2i2c(0x08,state);
}

void RevC_ERMPWMDuty(char state)
{
	send2i2c(0x09,state);
}
//IRQ

void RevC_IRQFilterPeriod(char state)
{
	send2i2c(0x0A,state);
}

void RevC_IRQPersistence(char state)
{
	send2i2c(0x0B,state);
}

void RevC_IRQPersistencePeriod(char state)
{
	send2i2c(0x0C,state);
}
//Enables

void RevC_LCDEnable(char state)
{
	send2i2c(0x0D,state);
}

void RevC_LCDReset(char state)
{
	send2i2c(0x0F,state);
}

void RevC_FIFOI2CEnable(char state)
{
	send2i2c(0x11,state);
}

void RevC_FIFOLCDEnable(char state)
{
	send2i2c(0x12,state);
}

void RevC_Reset2Defaults()
{
	send1i2c(0x13);

}
//

void RevC_SetTOUCHFlags(char state)
{
	send2i2c(0x0E,state);
}

void RevC_SetI2CWatchdogCounter(char state)
{
	send2i2c(0x1C,state);
}

void RevC_EnableI2CWatchdogCounter(char state)
{
	send2i2c(0x1D,state);
}

void RevC_GetTouchRAWXY()
{
	send1i2c(0x15);
}

void RevC_GetFIFOFull()
{
	send1i2c(0x16);
}

void RevC_ResetFIFOFull()
{
	send1i2c(0x17);
}
void RevC_GetTouchRAWZ1Z2()
{
	send1i2c(0x18);
}
#endif


void _UART_I_PRINT(int pcFormat)
{
	char cc[10];
	sprintf(cc,"%u",pcFormat);
	UART_PRINT(cc);
}
void _UART_I_PRINTLN(int pcFormat)
{
	char cc[10];
	sprintf(cc,"%u",pcFormat);
	UART_PRINT(cc);
	UART_PRINT("\n\r");
}
void _UART_C_PRINT(const char *pcFormat)
{
	UART_PRINT(pcFormat);
}
void _UART_C_PRINTLN(const char *pcFormat)
{
	UART_PRINT(pcFormat);
	UART_PRINT("\n\r");
}
void _UART_H_PRINT(char pcFormat)
{
	char cc[3];
	sprintf(cc,"%02X",pcFormat);
	UART_PRINT("0x");
	UART_PRINT(cc);
}
void _UART_H_PRINTLN(char pcFormat)
{
	char cc[3];
	sprintf(cc,"%02X",pcFormat);
	UART_PRINT("0x");
	UART_PRINT(cc);
	UART_PRINT("\n\r");
}
void _UART_L_PRINT(unsigned long x)
{
	const char * p = (const char*)&x;
	UART_PRINT(p);
}
void _UART_L_PRINTLN(unsigned long x)
{
	const char * p = (const char*)&x;
	UART_PRINT(p);
	UART_PRINT("\n\r");
}





