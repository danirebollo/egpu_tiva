/*
 * LCDBPV2_backlight.c
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#include <stdint.h>
#include <stdbool.h>

#include "LCDBPV2_setup.h"
#include "LCDBPV2_backlight.h"

#ifdef CC3200
#include "hw_types.h"
#include "hw_memmap.h"
#include "i2c_if.h"

void LCDBPV2_BacklightInit()
{
#ifdef LCDBPV2RevC
	RevC_BKForceEnable(0x01);
#else
	//enable LCDBPV2 Backlight
	//***********************************************************************************
	unsigned char aucDataBuf[256];
	aucDataBuf[0]=0x10;
	aucDataBuf[1]=0x00;
	aucDataBuf[2]=0xFF;
	aucDataBuf[3]=0x00;
	aucDataBuf[4]=0x44;
	aucDataBuf[5]=0x44;
	aucDataBuf[6]=0x44;
	aucDataBuf[7]=0x44;
	aucDataBuf[8]=0x44;
	aucDataBuf[9]=0xFF;
	aucDataBuf[10]=0x00;

	I2C_IF_Write(BACKLIGHT_I2C_ADDRESS, aucDataBuf, 11, true);
#endif


}
#else

#if defined(CC3200)||defined(TIVA)||defined(TIVACONNECTED)
	#include "inc/hw_types.h"
	#include "inc/hw_memmap.h"
	#include "driverlib/sysctl.h"
	#include "driverlib/gpio.h"
	#include "driverlib/i2c.h"
#elif defined(MSP430G2)

#endif





void LCDBPV2_BacklightState(char state)
{
#ifdef LCDBPV2RevC
	RevC_BKForceEnable(state);
#else

#endif
}
void LCDBPV2_BacklightPulse()
{
#ifdef LCDBPV2RevC
	RevC_BKPulse();
#else

#endif
}

void LCDBPV2_BacklightPulsePeriod(char state)
{
#ifdef LCDBPV2RevC
	RevC_BKPulsePeriod(state);
#else

#endif
}

void LCDBPV2_BacklightPWMFreq(char state)
{
#ifdef LCDBPV2RevC
	RevC_BKPWMFreq(state);
#else

#endif
}

void LCDBPV2_BacklightPWMDuty(char state)
{
#ifdef LCDBPV2RevC
	RevC_BKPWMDuty(state);
#else

#endif
}
void LCDBPV2_BacklightInit()
{
	//enable LCDBPV2 Backlight
	//***********************************************************************************
#ifdef LCDBPV2RevC
	RevC_BKForceEnable(0x01);
#else
	LCD_backlightInitRevAB();
#endif

}
#endif










