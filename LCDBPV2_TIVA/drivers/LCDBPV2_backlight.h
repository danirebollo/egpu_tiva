/*
 * LCDBPV2_backlight.h
 *
 *  Created on: 27/12/2013
 *      Author: Dani
 */

#ifndef LCDBPV2_BACKLIGHT_H_
#define LCDBPV2_BACKLIGHT_H_
#ifdef __cplusplus
extern "C"
{
#endif

void LCDBPV2_BacklightInit();
void LCDBPV2_BacklightPWMDuty(char state);
void LCDBPV2_BacklightPWMFreq(char state);
void LCDBPV2_BacklightPulsePeriod(char state);
void LCDBPV2_BacklightPulse();
void LCDBPV2_BacklightState(char state);

#ifdef __cplusplus
}
#endif
#endif /* LCDBPV2_BACKLIGHT_H_ */
