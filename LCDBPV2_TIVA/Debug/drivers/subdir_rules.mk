################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
drivers/LCDBP320x240x16_SSD1289.obj: C:/ti/LCDBPV2/drivers/LCDBP320x240x16_SSD1289.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/include" --include_path="C:/ti/LCDBPV2" --include_path="C:/ti/TivaWare/utils" --include_path="C:/ti/TivaWare/examples/boards/ek-tm4c123gxl" --include_path="C:/ti/TivaWare" --include_path="C:/ti/TivaWare/grlib" -g --gcc --define=ccs="ccs" --define=TIVA --define=PART_TM4C123GH6PM --define=TARGET_IS_TM4C123_RB1 --display_error_number --diag_warning=225 --diag_wrap=off --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="drivers/LCDBP320x240x16_SSD1289.pp" --obj_directory="drivers" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

drivers/LCDBPV2_MCU.obj: C:/ti/LCDBPV2/drivers/LCDBPV2_MCU.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/include" --include_path="C:/ti/LCDBPV2" --include_path="C:/ti/TivaWare/utils" --include_path="C:/ti/TivaWare/examples/boards/ek-tm4c123gxl" --include_path="C:/ti/TivaWare" --include_path="C:/ti/TivaWare/grlib" -g --gcc --define=ccs="ccs" --define=TIVA --define=PART_TM4C123GH6PM --define=TARGET_IS_TM4C123_RB1 --display_error_number --diag_warning=225 --diag_wrap=off --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="drivers/LCDBPV2_MCU.pp" --obj_directory="drivers" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

drivers/LCDBPV2_backlight.obj: C:/ti/LCDBPV2/drivers/LCDBPV2_backlight.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/include" --include_path="C:/ti/LCDBPV2" --include_path="C:/ti/TivaWare/utils" --include_path="C:/ti/TivaWare/examples/boards/ek-tm4c123gxl" --include_path="C:/ti/TivaWare" --include_path="C:/ti/TivaWare/grlib" -g --gcc --define=ccs="ccs" --define=TIVA --define=PART_TM4C123GH6PM --define=TARGET_IS_TM4C123_RB1 --display_error_number --diag_warning=225 --diag_wrap=off --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="drivers/LCDBPV2_backlight.pp" --obj_directory="drivers" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

drivers/LCDBPV2_setup.obj: C:/ti/LCDBPV2/drivers/LCDBPV2_setup.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/include" --include_path="C:/ti/LCDBPV2" --include_path="C:/ti/TivaWare/utils" --include_path="C:/ti/TivaWare/examples/boards/ek-tm4c123gxl" --include_path="C:/ti/TivaWare" --include_path="C:/ti/TivaWare/grlib" -g --gcc --define=ccs="ccs" --define=TIVA --define=PART_TM4C123GH6PM --define=TARGET_IS_TM4C123_RB1 --display_error_number --diag_warning=225 --diag_wrap=off --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="drivers/LCDBPV2_setup.pp" --obj_directory="drivers" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

drivers/LCDBPV2_touch.obj: C:/ti/LCDBPV2/drivers/LCDBPV2_touch.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O2 --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.2/include" --include_path="C:/ti/LCDBPV2" --include_path="C:/ti/TivaWare/utils" --include_path="C:/ti/TivaWare/examples/boards/ek-tm4c123gxl" --include_path="C:/ti/TivaWare" --include_path="C:/ti/TivaWare/grlib" -g --gcc --define=ccs="ccs" --define=TIVA --define=PART_TM4C123GH6PM --define=TARGET_IS_TM4C123_RB1 --display_error_number --diag_warning=225 --diag_wrap=off --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="drivers/LCDBPV2_touch.pp" --obj_directory="drivers" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


