/*
 * myUART.c
 *
 *  Created on: 10/10/2015
 *      Author: Dani
 */

#include "myUART.h"
#include "drivers/LCDBPV2_touch.h"

void initUART()
{
	/*
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	    GPIOPinConfigure(GPIO_PA0_U0RX);
	    GPIOPinConfigure(GPIO_PA1_U0TX);
	    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	    ROM_UARTConfigSetExpClk(UART0_BASE, ROM_SysCtlClockGet(), 115200, UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);
	    //UARTprintf("START");
	  */
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	    	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	    	ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	    	ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	    	ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	    	ROM_UARTConfigSetExpClk(UART0_BASE, ROM_SysCtlClockGet(), 115200,
	    			(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

	    	UARTStdioConfig(0, 115200, ROM_SysCtlClockGet());

	    	UARTprintf("START");
}
void message()
{
	UARTprintf("\n");
	UARTprintf("A: Send one to LCDBPV3. B: Send 5 to LCDBPV3 \n");
	UARTprintf("C: Send one to unknown. D: Send 5 to unknown \n");
	UARTprintf("E: Receive one from LCDBPV3. F: Receive 5 from LCDBPV3 \n");
	UARTprintf("G: Receive one from unknown. H: Receive 5 from unknown \n");

	UARTprintf("I: Receive XY TOUCH from LCDBPV3. J: Receive RAW XY TOUCH from LCDBPV3. \n");
	UARTprintf("K: Receive Z TOUCH from LCDBPV3.  \n");
	UARTprintf("- - - - - - - - - - - - - - - - - \n");
	UARTprintf("L: BK pulse.  \n");

	UARTprintf("a: Vibrator pulse. b: Backlight ON. \n");
	UARTprintf("c: Backlight OFF. \n");

	/*
				void LCDBPV2_VibratorPulse();
				void LCDBPV2_DefaultValues();
				void LCDBPV2_BacklightState(char state);
				void LCDBPV2_BacklightPulse();
				void LCDBPV2_BacklightPulsePeriod(char state);
				void LCDBPV2_BacklightPWMFreq(char state);
				void LCDBPV2_BacklightPWMDuty(char state);
				void LCDBPV2_VibratorState(char state);
				void LCDBPV2_VibratorPulsePeriod(char state);
				void LCDBPV2_VibratorPWMFreq(char state);
				void LCDBPV2_VibratorPWMDuty(char state);
				void LCDBPV2_IRQFilterPeriod(char state);
				void LCDBPV2_IRQPersistenceState(char state);
				void LCDBPV2_IRQPersistencePeriod(char state);
				void LCDBPV2_LCDEnable(char state);
				void LCDBPV2_LCDReset(char state);
				void LCDBPV2_FIFOI2CState(char state);
				void LCDBPV2_FIFOLCDState(char state);
				char LCDBPV2_GetFIFOLCDFullFlag();
				void LCDBPV2_ResetFIFOLCDState();
				void LCDBPV2_SetI2CWatchdogCounter(char counter);
				void LCDBPV2_EnableI2CWatchdogCounter(char state);
*/

}
void initUARTmessage()
{
	UARTCharPut(UART0_BASE, '\n');

	message();
}
void processUART()
{
	char buff[3];
	unsigned char aucDataBuf[10];

	if (UARTCharsAvail(UART0_BASE))
	{
		char uartchar;
		uartchar=UARTCharGet(UART0_BASE);
		UARTCharPut(UART0_BASE, ':');
		UARTCharPut(UART0_BASE, uartchar);
		UARTCharPut(UART0_BASE, ':');
		UARTprintf("\n");
		if(uartchar==13)
		{
			UARTprintf("Enter");
		}
		if(uartchar=='A')
		{
			sendi2c(1,LCDBPV3ADDR);
		}
		if(uartchar=='B')
		{
			sendi2c(5,LCDBPV3ADDR);
		}
		if(uartchar=='C')
		{
			sendi2c(1,0x50);
		}
		if(uartchar=='D')
		{
			sendi2c(5,0x50);
		}

		if(uartchar=='E')
		{
			receivei2c(1,LCDBPV3ADDR);
		}
		if(uartchar=='F')
		{
			receivei2c(4,LCDBPV3ADDR);
		}
		if(uartchar=='G')
		{
			receivei2c(1,0x51);
		}
		if(uartchar=='H')
		{
			receivei2c(4,0x51);
		}

		if(uartchar=='I')
		{
			I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, false);
			int bint;
			char i2c_command;

			const char *hexstring=(const char*) buff;

			strcpy(buff,"14");
			hexstring=buff;
			bint = (int)strtol(hexstring, NULL, 16);
			i2c_command=bint;
			aucDataBuf[0]=i2c_command;
			I2CMasterDataPut( I2C_M_BASE, aucDataBuf[0]);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
			WaitI2CDone( I2C_M_BASE);
			SysCtlDelay(SysCtlClockGet()/32);

			unsigned char d0, d1, d2, d3;
			I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, true);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d0 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d1 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d2 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d3 = I2CMasterDataGet(I2C_M_BASE);

			UARTprintf("Received data: ");

			char x0=d0;
			int x1=((char)((d1 & 0xF0) >> 4))<<8;

			char y0=(char) (d1 & 0x0F);
			int y1=d2<<4;
			UARTprintf("\n");
			UART_C_PRINT("X: ");
			int xraw=x0+x1;
			UART_I_PRINTLN(xraw);
			UART_C_PRINT("Y: ");
			int yraw=y0+y1;
			UART_I_PRINTLN(yraw);
			UART_C_PRINT("Z: ");
			UART_I_PRINTLN(d3);

			UARTprintf("\n");

		}

		if(uartchar=='J')
		{
			RevC_GetTouchRAWXY();

					char* data;
					data=LCDBPV2_Get4();

					//data[1]=0;
			// Tanto data[0] como data[1] crecen hasta la mitad de la pantalla y luego empiezan de 0 nuevamente
					//probar/comparar con codigo revc?
					uint16_t tYc, tXc;

					tX=(data[0]+(((data[1] & 0xF0) >> 4)<<8));
					tY=((data[1] & 0x0F)+(data[2]<<4));


					//LCDBPV2_calcParam();
					//if(tY<0) tY=0;
					//if(tY>LCD_MAX_Y) tY=LCD_MAX_Y;
					//if(tX<0) tX=0;
					//if(tX>LCD_MAX_X) tX=LCD_MAX_X;

					tXc=tX;
					tYc=tY;
			UARTprintf("Received data: ");

			UARTprintf("\n");
			UART_C_PRINT("X: ");
			UART_I_PRINTLN(tXc);
			UART_C_PRINT("Y: ");
			UART_I_PRINTLN(tYc);
			UART_C_PRINT("Z: ");
			UART_I_PRINTLN(data[3]);

			UARTprintf("\n");
		}

		if(uartchar=='K')
		{
			I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, false);
			int bint;
			char i2c_command;

			const char *hexstring=(const char*) buff;

			strcpy(buff,"18");
			hexstring=buff;
			bint = (int)strtol(hexstring, NULL, 16);
			i2c_command=bint;
			aucDataBuf[0]=i2c_command;
			I2CMasterDataPut( I2C_M_BASE, aucDataBuf[0]);
			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
			WaitI2CDone( I2C_M_BASE);
			SysCtlDelay(SysCtlClockGet()/32);

			unsigned char d0, d1, d2, d3;

			I2CMasterSlaveAddrSet( I2C_M_BASE, LCDBPV3ADDR, true);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d0 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d1 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d2 = I2CMasterDataGet(I2C_M_BASE);

			I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			while(I2CMasterBusy(I2C_M_BASE));
			//while(I2CMasterBusy(I2C_M_BASE));
			d3 = I2CMasterDataGet(I2C_M_BASE);

			UARTprintf("Received data: ");

			char x0=d0;
			int x1=((char)((d1 & 0xF0) >> 4))<<8;

			char y0=(char) (d1 & 0x0F);
			int y1=d2<<4;
			UARTprintf("\n");
			UART_C_PRINT("Z1: ");
			int xraw=x0+x1;
			UART_I_PRINTLN(xraw);
			UART_C_PRINT("Z2: ");
			int yraw=y0+y1;
			UART_I_PRINTLN(yraw);
			UART_C_PRINT(": ");
			UART_I_PRINTLN(d3);

			UARTprintf("\n");
		}
		if(uartchar=='L')
		{
			//BK Force enable
			send2i2c("00","01");
		}
		if(uartchar=='M')
		{
			//BK pulse
			send1i2c("01");
		}


		if(uartchar=='a')
		{
			LCDBPV2_VibratorPulse();
		}
		if(uartchar=='b')
				{
			LCDBPV2_BacklightState(1);
							}
		if(uartchar=='c')
						{
					LCDBPV2_BacklightState(0);
									}

		//send1i2c(char message[3]);
		//void send2i2c(char message1[3],char message2[3]);

		message();


		UARTprintf("X: ");
		UART_I_PRINT(tX);
		UARTprintf("  Y: ");
		UART_I_PRINT(tY);
		UARTprintf("\n");
	}
}
void sendi2c(int ni2c, char addr)
{
	unsigned char aucDataBuf[10];
	char buff[3];
	int ni2c2;
	int bint;
	char i2c_command,i2c_d;

	const char *hexstring=(const char*) buff;
	do{
		I2CMasterSlaveAddrSet( I2C_M_BASE, addr, false);
		//send command byte
		if(ni2c==0)
		{
		}
		else if(ni2c==1)
		{
			UARTprintf("Enter I2C Command: ");
			UARTgets(buff, sizeof(buff));
			hexstring=buff;
			bint = (int)strtol(hexstring, NULL, 16);
			i2c_command=bint;
			aucDataBuf[0]=i2c_command;

			if(memcmp(buff, "EX", 2) != 0)
			{
				I2CMasterDataPut( I2C_M_BASE, aucDataBuf[0]);
				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_SEND);
				WaitI2CDone( I2C_M_BASE);
			}
		}
		else
		{
			for(ni2c2=0;ni2c2<ni2c;ni2c2++)
			{
				UARTprintf("Enter I2C D");
				UART_I_PRINT(ni2c2);
				UARTprintf(": ");

				UARTgets(buff, sizeof(buff));
				hexstring=buff;
				bint = (int)strtol(hexstring, NULL, 16);
				i2c_d=bint;
				aucDataBuf[ni2c2]=i2c_d;
				//if(memcmp(buff, "EX", 2) != 0)
				//{
			}
			for(ni2c2=0;ni2c2<ni2c;ni2c2++)
			{
				I2CMasterDataPut( I2C_M_BASE, aucDataBuf[ni2c2]);
				if(ni2c2==0)
				{
					I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_START);
				}
				else if(ni2c2==(ni2c-1))
				{
					I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
				}
				else
				{
					I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
				}
				WaitI2CDone( I2C_M_BASE);
			}
			//else
			//{
			//	break;
			//}
			memcpy( buff, "EX", 2 );
		}
	}while(memcmp(buff, "EX", 2) != 0);
}
void receivei2c(int ni2c, char addr)
{
	unsigned char datah;
	//unsigned char datal;

	int ni2c2;
	//int bint;
	//char i2c_command,i2c_d;

	//const char *hexstring=(const char*) buff;

	//do{
	I2CMasterSlaveAddrSet( I2C_M_BASE, addr, true);   // false = write, true = read
	//send command byte
	if(ni2c==0)
	{
	}
	else if(ni2c==1)
	{
		//read data byte 1
		I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
		while(I2CMasterBusy(I2C_M_BASE));
		while(I2CMasterBusy(I2C_M_BASE));
		datah = I2CMasterDataGet(I2C_M_BASE);

		UARTprintf("Received data: ");
		UART_C_PRINTLN((const char*)datah);
		UARTprintf("\n");
	}
	else
	{
		for(ni2c2=0;ni2c2<ni2c;ni2c2++)
		{
			if(ni2c2==0)
			{
				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			}
			else if(ni2c2==(ni2c-1))
			{
				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			}
			else
			{
				I2CMasterControl( I2C_M_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			}

			while(I2CMasterBusy(I2C_M_BASE));
			while(I2CMasterBusy(I2C_M_BASE));
			datah = I2CMasterDataGet(I2C_M_BASE);

			UARTprintf("Received data: ");

			UART_H_PRINTLN(datah);
			UARTprintf("\n");

		}
		//else
		//{
		//	break;
		//}
		//memcpy( buff, "EX", 2 );
	}
	//}while(memcmp(buff, "EX", 2) != 0);
}
/////////////////////////////////

void UART_I_PRINT(int pcFormat)
{
	char cc[10];
	sprintf(cc,"%u",pcFormat);
	UARTprintf(cc);
}

void UART_I_PRINTLN(int pcFormat)
{
	char cc[10];
	sprintf(cc,"%u",pcFormat);
	UARTprintf(cc);
	UARTprintf("\n\r");
}

void UART_C_PRINT(const char *pcFormat)
{
	UARTprintf(pcFormat);
}

void UART_C_PRINTLN(const char *pcFormat)
{
	UARTprintf(pcFormat);
	UARTprintf("\n\r");
}

void UART_H_PRINT(char pcFormat)
{
	char cc[3];
	sprintf(cc,"%02X",pcFormat);
	UARTprintf("0x");
	UARTprintf(cc);
}

void UART_H_PRINTLN(char pcFormat)
{
	char cc[3];
	sprintf(cc,"%02X",pcFormat);
	UARTprintf("0x");
	UARTprintf(cc);
	UARTprintf("\n\r");
}

void UART_L_PRINT(unsigned long x)
{
	const char * p = (const char*)&x;
	UARTprintf(p);
}
void UART_L_PRINTLN(unsigned long x)
{
	const char * p = (const char*)&x;
	UARTprintf(p);
	UARTprintf("\n\r");
}

