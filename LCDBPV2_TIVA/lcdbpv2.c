#include <stdint.h>
#include <stdbool.h>

#include "stdio.h"

#include <time.h>
//#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
//#include "inc/hw_ints.h"

#include "utils/ustdlib.h"

#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/udma.h"

#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/pushbutton.h"

#include "drivers/LCDBPV2_setup.h"
#include "drivers/LCDBP320x240x16_SSD1289.h"
#include "drivers/LCDBPV2_backlight.h"
#include "drivers/LCDBPV2_touch.h"

#include "myUART.h"

//LCDBPV2

extern tContext sContext;
extern tRectangle sRect;
//widget example
extern tCanvasWidget g_sBackground;
extern tPushButtonWidget g_sPushBtn;
//extern void makeCal();
extern const uint8_t g_pui8Image[];
//extern void ClrScreen(int color);



// *****************************************************************************
//
// The DMA control structure table.
//
// *****************************************************************************
#ifdef ewarm
#pragma data_alignment=1024
tDMAControlTable sDMAControlTable[64];
#elif defined(ccs)
//#pragma DATA_ALIGN(sDMAControlTable, 1024)
tDMAControlTable sDMAControlTable[64];
#else
tDMAControlTable sDMAControlTable[64] __attribute__ ((aligned(1024)));
#endif
////////////////////////////////////////////////////////////////////////////////

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

//tDMAControlTable sDMAControlTable[64];

//*******WIDGET EXAMPLE
void OnButtonPress(tWidget *pWidget);

Canvas(g_sHeading, &g_sBackground, 0, &g_sPushBtn,
		&g_sLCDBP320x240x16_SSD1289, 0, 0, 320, 25,
		(CANVAS_STYLE_FILL | CANVAS_STYLE_OUTLINE | CANVAS_STYLE_TEXT),
		ClrBlack, ClrWhite, ClrRed, g_psFontCm20, "LED Control", 0, 0);

Canvas(g_sBackground, WIDGET_ROOT, 0, &g_sHeading,
		&g_sLCDBP320x240x16_SSD1289, 0, 25, 320, (240-25 ),
		CANVAS_STYLE_FILL, ClrBlack, 0, 0, 0, 0, 0, 0); //(240 - 23)

RectangularButton(g_sPushBtn, &g_sHeading, 0, 0,
		&g_sLCDBP320x240x16_SSD1289, 60, 60, 200, 40,
		(PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
				PB_STYLE_FILL), ClrGray, ClrWhite, ClrRed, ClrRed,
				g_psFontCmss22b, "Toggle red LED", 0, 0, 0, 0, OnButtonPress);

bool g_RedLedOn = false;
void OnButtonPress(tWidget *pWidget)
{
    g_RedLedOn = !g_RedLedOn;

    if(g_RedLedOn)
    {
        GPIOPinWrite(LED_PORT, RED_LED, RED_LED);
    }
    else
    {
        GPIOPinWrite(LED_PORT, RED_LED, 0x00);
    }
    LCDBPV2_VibratorPulse();
}




int main(void)
{

	FPUEnable();
	FPULazyStackingEnable();

    // Setup the system clock to run at 50 Mhz from PLL with crystal reference
    SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
    /// Configure and enable uDMA
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    SysCtlDelay(10);
    uDMAControlBaseSet(&sDMAControlTable[0]);
    uDMAEnable();

    //Enable LEDs PERIPHEAL PORT
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

    //Enable UART
    initUART();
    initUARTmessage();

    // enable led pins
    GPIOPinTypeGPIOOutput(LED_PORT, RED_LED|BLUE_LED|GREEN_LED);
    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, 0x00);
   // ROM_SysCtlClockGet();
    SysCtlDelay(TIME_MS*10);

    LCDBPV2_setup(TOUCH_SIMPLE);

    RevC_BKForceEnable(0);
    SysCtlDelay(TIME_MS*100);
    RevC_BKForceEnable(1);

 	//draw "LCDBPV2" text
    GrContextFontSet(&sContext, &g_sFontCm30b);
    GrContextForegroundSet(&sContext, ClrRed);

    //GrLineDraw(&sContext, 0, 0, 319, 0);
    //GrLineDraw(&sContext, 0, 0, 0, 239);

    GrContextForegroundSet(&sContext, ClrRed);
    GrLineDrawH(&sContext, 0, 319, 0);
    GrLineDrawH(&sContext, 0, 319, 239);

    //GrLineDrawH(&sContext, 0, 315, 6);
    GrContextForegroundSet(&sContext, ClrBlue);

    RevC_BKForceEnable(0);
        SysCtlDelay(100000);
        RevC_BKForceEnable(1);

    GrLineDrawV(&sContext, 0, 1, 239);
    GrLineDrawV(&sContext, 319, 1, 239);
    //GrLineDraw(&sContext, 0, 0,319, 239);

    sRect.i16XMin = 1;
        sRect.i16YMin = 1;
        sRect.i16XMax = 318;
        sRect.i16YMax = 238;
        GrContextForegroundSet(&sContext, ClrRed);
        //GrContextFontSet(&sContext, &g_sFontCmss30b);
        SysCtlDelay(SysCtlClockGet()/4);
        //GrStringDraw(&sContext, "Texas", -1, 110, 2, 0);
        //GrStringDraw(&sContext, "Instruments", -1, 80, 32, 0);
        //GrStringDraw(&sContext, "Graphics", -1, 100, 62, 0);
        //GrStringDraw(&sContext, "Lab", -1, 135, 92, 0);
        GrContextForegroundSet(&sContext, ClrBlack);
        GrRectFill(&sContext, &sRect);

        GrContextForegroundSet(&sContext, ClrYellow);
        sRect.i16XMin = 1;
        sRect.i16YMin = 100;
        sRect.i16XMax = 318;
        sRect.i16YMax = 238;
        GrRectDraw(&sContext, &sRect);
        GrContextForegroundSet(&sContext, ClrRed);
        GrCircleFill(&sContext, 150, 150, 50);

    //while(1);
    GrStringDrawCentered(&sContext, "LCDBPV2", -1,GrContextDpyWidthGet(&sContext) / 2, 80, 0);
    SysCtlDelay(TIME_MS);

/*
    //draw image
    GrImageDraw(&sContext, g_pui8Image, 0, 0);
    GrFlush(&sContext);
    SysCtlDelay(SysCtlClockGet()/4);

    //draw banner
    ClrScreen(ClrBlack);
    sRect.i16XMin = 1;
    sRect.i16YMin = 1;
    sRect.i16XMax = 318;
    sRect.i16YMax = 238;
    GrContextForegroundSet(&sContext, ClrRed);
    GrContextFontSet(&sContext, &g_sFontCmss30b);
    SysCtlDelay(SysCtlClockGet()/4);
    GrStringDraw(&sContext, "Texas", -1, 110, 2, 0);
    GrStringDraw(&sContext, "Instruments", -1, 80, 32, 0);
    GrStringDraw(&sContext, "Graphics", -1, 100, 62, 0);
    GrStringDraw(&sContext, "Lab", -1, 135, 92, 0);
    GrContextForegroundSet(&sContext, ClrWhite);
    GrRectDraw(&sContext, &sRect);
    GrFlush(&sContext);
    SysCtlDelay(SysCtlClockGet()/4);

    //white screen & led
    ClrScreen(ClrWhite);
    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, RED_LED|BLUE_LED|GREEN_LED);
    SysCtlDelay(SysCtlClockGet()/8);
    //red screen & led
    ClrScreen(ClrRed);
    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, RED_LED);
    SysCtlDelay(SysCtlClockGet()/8);
    //green screen & led
    ClrScreen(ClrGreen);
    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, GREEN_LED);
    SysCtlDelay(SysCtlClockGet()/8);
 */
    //blue screen & led
    ClrScreen(ClrBlue);
    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, BLUE_LED);
    SysCtlDelay(SysCtlClockGet()/8);
    //black screen & led off


    ClrScreen(ClrBlack);

    GPIOPinWrite(LED_PORT, RED_LED|BLUE_LED|GREEN_LED, 0);

    /*
     * To Calibrate screen you need to use makeCal() function to get
     * "A", "B", "C", "D", "E", "F" and "Div" parameters for LCDBPV2_loadparm() function.
     * Once this is done you only need to call LCDBPV2_loadparm();
     */

    //run touch calibration and get parameters for LCDBPV2_loadparm()
    //makeCal();
    SysCtlDelay(SysCtlClockGet()/4);

    //widget example
    WidgetAdd(WIDGET_ROOT, (tWidget *)&g_sBackground);
    WidgetPaint(WIDGET_ROOT);

    while(1)
    {
    	WidgetMessageQueueProcess();

    	//SliderValueSet(&g_psSliders[0], value);
    	//WidgetPaint((tWidget *)&g_psSliders[0]);
    	processUART();
    }
}






