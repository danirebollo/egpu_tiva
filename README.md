# eGPU #

eGPU (formerly called LCDBPVx) is an embedded GPU for embedded systems.
The basic approach of eGPU is to provide an easy way to control a LCD screen with touch pannel, haptics and some other user interface like rotary encoders and keyboard.
Everything is controlled through I2C and EHA propietary protocol at the display side.

### In this repository you can found eGPU libraries for Tiva Launchpad board using Code Composer Studio. ###

### How do I get set up? ###
Simply fork this repository to your Code Composer Studio workspace.

### Contribution guidelines ###

* Test code 
* Request code improvements
* Report bugs

### Who do I talk to? ###

* If you have some doubts or need to talk in depth please use the danirebollo.es forum or support section or send an email to support@danirebollo.es 